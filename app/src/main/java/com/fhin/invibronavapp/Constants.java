package com.fhin.invibronavapp;

import java.util.Arrays;
import java.util.IllegalFormatException;
import java.util.stream.Collectors;

public class Constants {
    // MeasurementType[M,C,etc.];SensorName;DataSource[RAW,FILTERED];Data
    public static final String LOG_ENTRY_DATA_FORMAT = "%s;%s;%s;%s";
    public static final String LOG_HEADER_LINE = "LogType;Sensor;MeasurementType;Data";
    // Timestamp Source LogLevel: Message
    public static final String GENERAL_LOG_ENTRY_FORMAT = "[%1$tc]|%2$s|%4$s|%5$s%n";

    public static final float DEFAULT_GRAVITY = 9.81f;
    public static final int NUM_REQUIRED_ACCELEROMETER_CALIBRATION_MEASUREMENTS = 250;
    public static final int AZIMUTH_HISTORY_LENGTH = 20;
    public static final int VELOCITY_WINDOW_SIZE = 40;
    public static final int NUM_REQUIRED_CALC_HEADING_MEASUREMENTS = 6;
    public static final float ALLOWED_TARGET_ANGLE_DEVIATION = 7.5f;
    public static final float WAYPOINT_REACHED_THRESHOLD = 0.3f;
    public static final float SIGNIFICANT_AXIS_MOVEMENT_THRESHOLD = 0.1f;

    public static final byte DIRECTION_NORTH_FLAG = 0;
    public static final byte DIRECTION_NORTHEAST_FLAG = 1;
    public static final byte DIRECTION_EAST_FLAG = 2;
    public static final byte DIRECTION_SOUTHEAST_FLAG = 3;
    public static final byte DIRECTION_SOUTH_FLAG = 4;
    public static final byte DIRECTION_SOUTHWEST_FLAG = 5;
    public static final byte DIRECTION_WEST_FLAG = 6;
    public static final byte DIRECTION_NORTHWEST_FLAG = 7;
    public static final byte DIRECTION_INVALID_FLAG = 8;

    public static String DEFAULT_HOSTNAME = "";
    public static final int NO_SELECTED_LIST_ITEM_IDX = -1;
    public static final int ERROR_SELECTING_LOCATION = -2;
    public static final int ERROR_SELECTING_SHOP = -2;
    public static final int INVALID_LOCATION_ID = -1;
    public static final int INVALID_SHOP_ID = -1;
    public static final double INVALID_LOCATION_COORDINATE_VALUE = -1;
    
    public static final String EXTRA_SELECTED_LOCATION_ID = "shopSelectionSelectedLocationId";
    public static final String EXTRA_SELECTED_SHOP_ID = "shopSelectionSelectedShopId";
    public static final String EXTRA_LOCATION_X = "guidingLocationX";
    public static final String EXTRA_LOCATION_Y = "guidingLocationY";
    public static final String EXTRA_WAYPOINTS_TO_TARGET = "waypointsToTarget";
    public static final String EXTRA_GUIDING_LOCATION_TARGET_NAME = "guidingTargetName";
    public static final String INVALID_SHOP_NAME = "<noTarget>";

    public static final int GUIDING_SERVICE_ID = 4711;
    public static final String GUIDING_SERVICE_NOTIFICATION_CHANNEL_ID = "1337";
    public static final String GUIDING_SERVICE_NOTIFICATION_CHANNEL_NAME = "guidingServiceNotificationChannel";
    public static final String GUIDING_SERVICE_NOTIFICATION_TITLE_FORMAT = "Tracking %s";
    public static final String GUIDING_SERVICE_NOTIFICATION_CONTENT_FORMAT = "Direction: %s | Distance: %s";

    public static final String ERR_NO_SHOP_FOR_GIVEN_ID = "There was no shop with the given id %d";
    public static final String ERR_NO_LOCATION_FOR_GIVEN_ID = "There was no location with the given id %d";

    public static final String ERR_INVALID_URL = "Given url %s is not a valid URL";
    public static final String ERR_NO_REQUEST_FOR_TYPE = "There was no request defined for the given type %s";
    public static final String ERR_FETCHING_SHOPS_FOR_LOCATION = "Error while fetching shops for location %d";
    public static final String ERR_PARSING_SHOPS_FOR_LOCATION_JSON_RESPONSE = "Error while parsing json response when fetching shop data for location";
    public static final String ERR_FETCHING_LOCATIONS = "Error while fetching available locations";
    public static final String ERR_PARSING_LOCATION_JSON_RESPONSE = "Error while parsing json response when fetching location data";

    public static final String ERR_INVALID_LOCATION_SELECTED = "Please select a valid location";
    public static final String ERR_INVALID_SHOP_SELECTED = "Please select a valid shop for your selected location";
    public static final String ERR_WHILE_SELECTING_LOCATION = "There was an error while you selected your location";
    public static final String ERR_WHILE_SELECTING_SHOP = "There was an error while you selected a shop for the given location";

    public static final String ERR_LOADING_PATH = "Error loading path to shop %s for location %s";

    // Activity strings
    public static final String INFO_PICK_LOCATION  = "Pick location";
    public static final String INFO_LOADING_LOCATIONS = "Loading locations...";
    public static final String INFO_LOADING_SHOPS_FOR_LOCATION = "Loading shops for location %s ...";
    public static final String INFO_INIT_GUIDING = "Initializing guiding";
    public static final String INFO_TRIGGER_MANUAL_LOCATION_FETCH = "Load locations";
    public static final String INFO_GUIDING_CALIBRATION_MSG = "Calibrating, please place the device on a stationary object or stand still...";
    public static final String INFO_HEADER_LOCATION_SELECTION = "Please select (tap and hold) your current location";
    public static final String INFO_HEADER_SHOP_SELECTION = "Please select (tap and hold) the shop you would like to visit";

    // Guiding service message constants
    public static final String ERR_NO_SENSOR_FOR_TYPE = "Device has no suitable sensor of type %d";
    public static final String INFO_SENSOR_ACCURACY_CHANGED = "Accuracy for sensor %s of type %d has changed to %d";
    public static final String INFO_PAUSING_SENSOR_LISTENING = "Pausing listening to sensor data";
    public static final String INFO_RESUMING_SENSOR_LISTENING = "Resuming listening to sensor data";
    public static final String INFO_SHUTDOWN_SENSOR_SERVICE = "Shutting down sensor service";
    public static final String ERR_CREATING_SENSOR_SERVICE = "Error creating sensor service";
    public static final String INFO_STARTING_SENSOR_SERVICE = "Starting sensor service";

    public static final String ERR_BUILDING_MESSAGE = "Error while building message with format %s and params %s";

    public static String BuildMessage(String msgFormat, Object param){
        return BuildMessage(msgFormat, new Object[]{param});
    }

    public static String BuildMessage(String msgFormat, Object[] params){
        try {
            return String.format(msgFormat, params);
        }
        catch (IllegalFormatException ex){
            String paramsAsString = Arrays.stream(params).map(obj ->
            {
                try
                {
                    return obj.toString();
                }
                catch(Exception stringifyEx)
                {
                    return "<nullOrErrorWhileStringify>";
                }
            }).collect(Collectors.joining(","));
            throw new IllegalArgumentException(BuildMessage(Constants.ERR_BUILDING_MESSAGE, new Object[]{msgFormat, paramsAsString}));
        }
        catch (NullPointerException ex){
            String paramsAsString = Arrays.stream(params).map(obj ->
            {
                try
                {
                    return obj.toString();
                }
                catch(Exception stringifyEx)
                {
                    return "<nullOrErrorWhileStringify>";
                }
            }).collect(Collectors.joining(","));
            throw new IllegalArgumentException(BuildMessage(Constants.ERR_BUILDING_MESSAGE, new Object[]{msgFormat, paramsAsString}));
        }
    }
}
