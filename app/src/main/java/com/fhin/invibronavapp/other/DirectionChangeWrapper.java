package com.fhin.invibronavapp.other;

import android.util.Log;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import com.fhin.invibronavapp.models.Direction;
import com.fhin.invibronavapp.models.DirectionChange;

public class DirectionChangeWrapper {
    private static volatile DirectionChangeWrapper instance;
    private static final Object LOCK = new Object();

    private Direction currentDeviceDirection;
    private Direction otherDirection;

    private static MutableLiveData<DirectionChange> notForDeviceDirectionLiveData;
    private static MutableLiveData<DirectionChange> forDeviceDirectionLiveData;


    protected DirectionChangeWrapper() {
        currentDeviceDirection = Direction.INVALID;
        otherDirection = Direction.INVALID;

        forDeviceDirectionLiveData = new MutableLiveData<>();
        notForDeviceDirectionLiveData = new MutableLiveData<>();
    }

    public static DirectionChangeWrapper getInstance(){
        if (instance == null){
            synchronized (LOCK){
                if (instance == null){
                    instance = new DirectionChangeWrapper();
                }
            }
        }
        return instance;
    }

    public void setNewDirection(boolean forDevice, Direction newDirection){
        DirectionChange directionChange;
        if (forDevice){
            directionChange = new DirectionChange(true, currentDeviceDirection, newDirection);
            currentDeviceDirection = newDirection;
            forDeviceDirectionLiveData.postValue(directionChange);
        }
        else {
            directionChange = new DirectionChange(false, otherDirection, newDirection);
            otherDirection = newDirection;
            notForDeviceDirectionLiveData.postValue(directionChange);
        }
    }

    public void registerObserver(boolean forDevice, LifecycleOwner observerLifeCycleOwner, Observer<DirectionChange> newDirectionChangeObserver){
        if (newDirectionChangeObserver == null) return;

        final MutableLiveData<DirectionChange> directionChangeLiveDataTarget = forDevice ? forDeviceDirectionLiveData : notForDeviceDirectionLiveData;
        if (observerLifeCycleOwner == null){
            directionChangeLiveDataTarget.observeForever(newDirectionChangeObserver);
        }
        else {
            directionChangeLiveDataTarget.observe(observerLifeCycleOwner, newDirectionChangeObserver);
        }
    }

    public void deregisterObserver(boolean forDevice, Observer<DirectionChange> directionChangeObserverToRemove){
        if (directionChangeObserverToRemove == null) return;
        final MutableLiveData<DirectionChange> directionChangeLiveDataTarget = forDevice ? forDeviceDirectionLiveData : notForDeviceDirectionLiveData;
        directionChangeLiveDataTarget.removeObserver(directionChangeObserverToRemove);
    }
}
