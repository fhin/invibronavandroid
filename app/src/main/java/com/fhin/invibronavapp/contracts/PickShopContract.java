package com.fhin.invibronavapp.contracts;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import androidx.activity.result.contract.ActivityResultContract;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.fhin.invibronavapp.Constants;
import com.fhin.invibronavapp.activities.shop.ShopSelectionActivity;

public class PickShopContract extends ActivityResultContract<Integer, ShopSelectionResult> {
    @NonNull
    @Override
    public Intent createIntent(@NonNull Context context, Integer selectedLocationId) {
        Intent pickShopIntent = new Intent(context, ShopSelectionActivity.class);
        pickShopIntent.putExtra(Constants.EXTRA_SELECTED_LOCATION_ID, selectedLocationId.intValue());
        return pickShopIntent;
    }

    @Override
    public ShopSelectionResult parseResult(int resultCode, @Nullable Intent intent) {
        int selectedLocationId = intent.getIntExtra(Constants.EXTRA_SELECTED_LOCATION_ID, Constants.INVALID_LOCATION_ID);
        int selectedShopId;

        switch (resultCode){
            case Activity.RESULT_OK:
                selectedShopId = intent.getIntExtra(Constants.EXTRA_SELECTED_SHOP_ID, Constants.INVALID_SHOP_ID);
                break;
            case Activity.RESULT_CANCELED:
                selectedShopId = Constants.INVALID_SHOP_ID;
                break;
            default:
                selectedShopId = Constants.ERROR_SELECTING_SHOP;
                break;
        }
        return new ShopSelectionResult(selectedLocationId, selectedShopId);
    }
}
