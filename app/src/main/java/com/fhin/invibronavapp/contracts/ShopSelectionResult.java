package com.fhin.invibronavapp.contracts;

public class ShopSelectionResult {
    public int locationId;
    public int shopId;

    public ShopSelectionResult(int locationId, int shopId){
        this.locationId = locationId;
        this.shopId = shopId;
    }
}
