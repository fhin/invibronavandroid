package com.fhin.invibronavapp.contracts;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import androidx.activity.result.contract.ActivityResultContract;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.fhin.invibronavapp.Constants;
import com.fhin.invibronavapp.activities.location.LocationSelectionActivity;

public class PickLocationContract extends ActivityResultContract<Void, Integer> {

    @NonNull
    @Override
    public Intent createIntent(@NonNull Context context, Void input) {
        Intent pickLocationIntent = new Intent(context, LocationSelectionActivity.class);
        return pickLocationIntent;
    }

    @Override
    public Integer parseResult(int resultCode, @Nullable Intent intent) {
        int selectedLocationId;
        switch (resultCode){
            case Activity.RESULT_OK:
                selectedLocationId = intent.getIntExtra(Constants.EXTRA_SELECTED_LOCATION_ID, Constants.INVALID_LOCATION_ID);
                break;
            case Activity.RESULT_CANCELED:
                selectedLocationId = Constants.INVALID_LOCATION_ID;
                break;
            default:
                selectedLocationId = Constants.ERROR_SELECTING_LOCATION;
                break;
        }
        return new Integer(selectedLocationId);
    }
}
