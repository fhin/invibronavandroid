package com.fhin.invibronavapp.activities.shop;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.widget.RecyclerView;

import com.fhin.invibronavapp.Constants;
import com.fhin.invibronavapp.R;
import com.fhin.invibronavapp.models.ShopInformation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

// https://developer.android.com/guide/topics/ui/layout/recyclerview
public class ShopSelectionAdapter extends RecyclerView.Adapter<ShopSelectionViewHolder>{
    private ShopInformation[] localShopData;
    private SelectionTracker selectionTracker;

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param data ShopModel[] containing the data to populate views to be used
     * by RecyclerView.
     */
    public ShopSelectionAdapter(Iterable<ShopInformation> data){
        ArrayList<ShopInformation> filteredShopData = new ArrayList<>();
        for (ShopInformation d : data){
            if (d == null) continue;
            filteredShopData.add(d);
        }
        localShopData = new ShopInformation[filteredShopData.size()];
        localShopData = filteredShopData.toArray(localShopData);
    }

    public ShopInformation getShopAtIndex(int index){
        if (index < 0 || index >= this.getItemCount()){
            throw new IndexOutOfBoundsException();
        }
        return localShopData[index];
    }

    public ShopInformation findShopForId(int shopId){
        for (int currIdx = 0; currIdx < getItemCount(); currIdx++){
            if (getShopAtIndex(currIdx).getId() == shopId){
                return localShopData[currIdx];
            }
        }
        throw new IllegalArgumentException(Constants.BuildMessage(Constants.ERR_NO_SHOP_FOR_GIVEN_ID, shopId));
    }

    public void setSelectionTracker(SelectionTracker selectionTracker){
        this.selectionTracker = selectionTracker;
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public ShopSelectionViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.shop_selection_list_item, parent, false);
        return new ShopSelectionViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ShopSelectionViewHolder viewHolder, final int position){
        viewHolder.Update(localShopData[position], selectionTracker.isSelected(localShopData[position]));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount(){
        return localShopData.length;
    }
}
