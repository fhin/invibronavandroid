package com.fhin.invibronavapp.activities.location;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.selection.ItemDetailsLookup;
import androidx.recyclerview.widget.RecyclerView;

import com.fhin.invibronavapp.R;
import com.fhin.invibronavapp.models.LocationInformation;

public class LocationSelectionViewHolder extends RecyclerView.ViewHolder{
    private int locationId;
    private ImageView locationIcon;
    private TextView locationName;
    private TextView locationAdress;
    private ImageButton locationInfoButton;

    public LocationSelectionViewHolder(@NonNull View view){
        super(view);

        locationIcon = view.findViewById(R.id.locationSelectIcon);
        locationName = view.findViewById(R.id.locationSelectionName);
        locationAdress = view.findViewById(R.id.locationSelectionAdress);
        locationInfoButton = view.findViewById(R.id.locationSelectInfoButton);
    }

    public void Update(LocationInformation model, boolean isActive){
        itemView.setActivated(isActive);
        locationId = model.getId();
        locationName.setText(model.getName());
        locationAdress.setText(model.getAdress());
    }

    public ItemDetailsLookup.ItemDetails getItemDetails(){
        return new ItemDetailsLookup.ItemDetails() {
            @Override
            public int getPosition() {
                return getAdapterPosition();
            }

            @Nullable
            @Override
            public String getSelectionKey() {
                return String.valueOf(locationId);
            }
        };
    }
}
