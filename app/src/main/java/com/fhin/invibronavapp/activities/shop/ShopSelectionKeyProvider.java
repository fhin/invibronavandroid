package com.fhin.invibronavapp.activities.shop;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.selection.ItemKeyProvider;

import com.fhin.invibronavapp.Constants;
import com.fhin.invibronavapp.models.ShopInformation;

public class ShopSelectionKeyProvider extends ItemKeyProvider<String> {
    private final ShopSelectionAdapter adapter;

    public ShopSelectionKeyProvider(ShopSelectionAdapter shopSelectionAdapter){
        super(ItemKeyProvider.SCOPE_CACHED);
        this.adapter = shopSelectionAdapter;
    }

    @Nullable
    @Override
    public String getKey(int position) {
        try {
            ShopInformation shopAtPosition = adapter.getShopAtIndex(position);
            return String.valueOf(shopAtPosition.getId());
        }
        catch (IndexOutOfBoundsException|NullPointerException ex){
            return null;
        }
    }

    @Override
    public int getPosition(@NonNull String key) {
        for (int currIdx = 0; currIdx < adapter.getItemCount(); currIdx++){
            if (getKey(currIdx).equals(key)){
                return currIdx;
            }
        }
        return Constants.NO_SELECTED_LIST_ITEM_IDX;
    }
}
