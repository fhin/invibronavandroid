package com.fhin.invibronavapp.activities.shop;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.selection.ItemDetailsLookup;
import androidx.recyclerview.widget.RecyclerView;

import com.fhin.invibronavapp.R;
import com.fhin.invibronavapp.models.ShopInformation;

public class ShopSelectionViewHolder extends RecyclerView.ViewHolder {
    private int shopId;
    private final ImageView shopIcon;
    private final TextView shopName;
    private final ImageButton shopInfoButton;

    public ShopSelectionViewHolder(@NonNull View view){
        super(view);

        shopIcon = view.findViewById(R.id.shopIcon);
        shopName = view.findViewById(R.id.shopName);
        shopInfoButton = view.findViewById(R.id.shopInfoButton);
    }

    public void Update(ShopInformation newShopModel, boolean isActive){
        itemView.setActivated(isActive);
        shopId = newShopModel.getId();
        shopName.setText(newShopModel.getName());
    }

    public ItemDetailsLookup.ItemDetails<String> getItemDetails(){
        return new ItemDetailsLookup.ItemDetails<String>() {
            @Override
            public int getPosition() {
                return getAdapterPosition();
            }

            @Nullable
            @Override
            public String getSelectionKey() {
                return String.valueOf(shopId);
            }
        };
    }
}
