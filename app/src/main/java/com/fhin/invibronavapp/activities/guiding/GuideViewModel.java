package com.fhin.invibronavapp.activities.guiding;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.fhin.invibronavapp.models.DirectionChange;
import com.fhin.invibronavapp.other.DirectionChangeWrapper;

public class GuideViewModel extends ViewModel {
    private Observer<DirectionChange> deviceDirectionChangeObserver;
    private Observer<DirectionChange> targetDirectionChangeObserver;

    private MutableLiveData<DirectionChange> deviceDirection;
    private MutableLiveData<DirectionChange> targetDirection;

    public GuideViewModel(){
        deviceDirection = new MutableLiveData<>();
        targetDirection = new MutableLiveData<>();

        deviceDirectionChangeObserver = directionChange -> deviceDirection.setValue(directionChange);
        targetDirectionChangeObserver = directionChange -> targetDirection.setValue(directionChange);

        DirectionChangeWrapper.getInstance().registerObserver(true, null, deviceDirectionChangeObserver);
        DirectionChangeWrapper.getInstance().registerObserver(false, null, targetDirectionChangeObserver);
    }

    public LiveData<DirectionChange> getDeviceDirection(){
        return deviceDirection;
    }

    public LiveData<DirectionChange> getTargetDirection(){
        return targetDirection;
    }
}
