package com.fhin.invibronavapp.activities;

import android.content.Context;
import android.widget.Toast;

public class Notification {
    public static void showToast(Context context, String msg, boolean showForShortDuration){
        if (msg == null || context == null) return;
        Toast toastToShow = Toast.makeText(context, msg, showForShortDuration ? Toast.LENGTH_SHORT : Toast.LENGTH_LONG);
        toastToShow.show();
    }
}
