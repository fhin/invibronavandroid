package com.fhin.invibronavapp.activities.guiding;

import com.fhin.invibronavapp.models.Direction;
import com.fhin.invibronavapp.models.DirectionChange;

public interface HeadingChangeListener {
    void onHeadingChanged(DirectionChange directionChange);
}
