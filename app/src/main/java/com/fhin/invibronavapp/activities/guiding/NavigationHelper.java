package com.fhin.invibronavapp.activities.guiding;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.util.Log;
import android.widget.Toast;

import com.fhin.invibronavapp.AxisIndex;
import com.fhin.invibronavapp.Constants;
import com.fhin.invibronavapp.activities.Notification;
import com.fhin.invibronavapp.models.Direction;
import com.fhin.invibronavapp.models.DirectionChange;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NavigationHelper {
    private final String TAG = "NavigationHelper";
    private final AccelerometerCalibration accelerometerCalibration;
    public final DeviceOrientationHelper deviceOrientationHelper;
    private final LinearAcceleration linearAcceleration;
    private final ArrayList<HeadingChangeListener> headingChangeListeners;
    private final Context context;
    private final Logger logger;

    private double currHeadingAngleInDegrees;

    public boolean isSetupDone(){
        return accelerometerCalibration.isDone();
    }

    // TODO: Support for landscape mode or different rotations in general (coordinates need to be remapped, SensorManager has some utility functions for that) [MORE RESEARCH]
    // see further: https://lh3.googleusercontent.com/eloZBMBLQ9o8CC_84rfN-JedVCdzStQhhbs1FImcgOEmUYoeijg-tS4xNh6Kmd234n4=w350
    public NavigationHelper(Logger logger, Context context, int numRequiredMeasurementForCalibration){
        this.logger = logger;
        this.context = context;

        if (numRequiredMeasurementForCalibration <= 0){
            numRequiredMeasurementForCalibration = Constants.NUM_REQUIRED_ACCELEROMETER_CALIBRATION_MEASUREMENTS;
        }
        accelerometerCalibration = AccelerometerCalibration.getInstance(numRequiredMeasurementForCalibration);
        deviceOrientationHelper = new DeviceOrientationHelper();
        linearAcceleration = new LinearAcceleration(0, 0, -Constants.DEFAULT_GRAVITY);
        headingChangeListeners = new ArrayList<>();

        currHeadingAngleInDegrees = -1;
    }


    public void handleSensorEvent(SensorEvent event){
        if (event == null) return;
        int sensorType = event.sensor.getType();
        switch (sensorType){
            case Sensor.TYPE_ACCELEROMETER:
                deviceOrientationHelper.UpdateAccelerometerReadings(event.values);
                if (!isSetupDone()){
                    accelerometerCalibration.handleSensorData(event.values);
                    if (accelerometerCalibration.isDone()){
                        float[] measuredGravity = accelerometerCalibration.getCalibratedValues();
                        linearAcceleration.updateCalibratedGravity(measuredGravity[0], measuredGravity[1], measuredGravity[2]);
                        logMeasurement(measuredGravity, "G", true);
                        String printableMeasuredGravityInfo = String.format("Measure gravity after calibration was finished: X: %f, Y: %f, Z: %f",
                                measuredGravity[AxisIndex.X.index],
                                measuredGravity[AxisIndex.Y.index],
                                measuredGravity[AxisIndex.Z.index]);
                        Notification.showToast(context, printableMeasuredGravityInfo, false);
                        Log.d(TAG, printableMeasuredGravityInfo);
                    }
                }
                else {
                    float[] measureAcceleration = event.values;
                    final float[] R = new float[16];
                    final float[] I = new float[16];
                    SensorManager.getRotationMatrix(R, I, deviceOrientationHelper.accelerometerReading, deviceOrientationHelper.magnetometerReading);
                    linearAcceleration.transformSensorData(R, I, measureAcceleration[0], measureAcceleration[1], measureAcceleration[2]);
                    /*
                    if (!linearAcceleration.wasSignificantMovement()){
                        break;
                    }
                    logCurrentMeasurement();
                    Log.d(TAG, String.format("Measured TRANSFORMED: X: %f, Y: %f, Z: %f",
                            linearAcceleration.X,
                            linearAcceleration.Y,
                            linearAcceleration.Z));

                     */
                }
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                deviceOrientationHelper.UpdateMagnetometerReadings(event.values);
                if (!isSetupDone()){
                    break;
                }
                double updatedHeadingInDegrees = deviceOrientationHelper.GetAzimuth();
                //Log.d(TAG, String.format("Current heading: %f", updatedHeadingInDegrees));
                Direction headingBeforeUpdate = getCurrentHeadingAsDirection();
                Direction updatedHeading = Direction.getDirectionFromAngle(updatedHeadingInDegrees);
                if (headingBeforeUpdate != updatedHeading) {
                    notifyAboutHeadingChange(headingBeforeUpdate, updatedHeading);
                    currHeadingAngleInDegrees = updatedHeadingInDegrees;
                }
                break;
            default:
                break;
        }
    }

    public BigDecimal[] getCurrentMovement(){
        BigDecimal[] currentTranslation = new BigDecimal[3];
        currentTranslation[AxisIndex.X.index] = linearAcceleration.X;
        currentTranslation[AxisIndex.Y.index] = linearAcceleration.Y;
        currentTranslation[AxisIndex.Z.index] = linearAcceleration.Z;
        return currentTranslation;
    }

    public Direction getCurrentHeadingAsDirection(){
        return Direction.getDirectionFromAngle(currHeadingAngleInDegrees);
    }

    private void notifyAboutHeadingChange(Direction oldDirection, Direction newDirection){
        for (HeadingChangeListener headingChangeListener : headingChangeListeners){
            if (headingChangeListener == null) continue;
            headingChangeListener.onHeadingChanged(new DirectionChange(true, oldDirection, newDirection));
        }
    }

    public void registerForHeadingChange(HeadingChangeListener headingChangeListener){
        if (headingChangeListener == null) return;
        headingChangeListeners.add(headingChangeListener);
    }

    private void logCurrentMeasurement(){
        logMeasurement(new float[] {linearAcceleration.X.floatValue(), linearAcceleration.Y.floatValue(), linearAcceleration.Z.floatValue()}, "M", true);
    }

    private void logMeasurement(float[] values, String TAG, boolean transformed){
        String[] printableMovement = new String[]{
                String.valueOf(values[AxisIndex.X.index]),
                String.valueOf(values[AxisIndex.Y.index]),
                String.valueOf(values[AxisIndex.Z.index])
        };
        String measurementText = String.join(",", printableMovement);
        logger.info(String.format(Constants.LOG_ENTRY_DATA_FORMAT, new Object[]{TAG, "ACC", transformed ? "TRANS" : "RAW", measurementText}));
    }
}
