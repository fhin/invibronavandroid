package com.fhin.invibronavapp.activities.location;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.selection.SelectionPredicates;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.selection.StorageStrategy;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fhin.invibronavapp.Constants;
import com.fhin.invibronavapp.R;
import com.fhin.invibronavapp.activities.Notification;
import com.fhin.invibronavapp.backendcommunication.ServiceFactory;
import com.fhin.invibronavapp.models.LocationInformation;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

public class LocationSelectionActivity extends AppCompatActivity {
    private TextView locationSelectHeader;
    private TextView locationSelectProgressMessage;
    private ProgressBar locationSelectProgressBar;

    private RecyclerView locationSelectRecyclerView;
    protected LocationSelectionAdapter locationSelectionAdapter;
    private SelectionTracker locationSelectionTracker;

    private ExecutorService backgroundWorkExecutor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_selection);

        backgroundWorkExecutor = Executors.newFixedThreadPool(1);

        locationSelectHeader = findViewById(R.id.locationSelectHeaderText);
        locationSelectHeader.setText(Constants.INFO_HEADER_LOCATION_SELECTION);

        locationSelectProgressMessage = findViewById(R.id.locationSelectProgressMessage);
        locationSelectProgressBar = findViewById(R.id.locationSelectProgressBar);

        locationSelectRecyclerView = findViewById(R.id.locationSelectRecyclerView);
        locationSelectRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        locationSelectRecyclerView.setVisibility(View.INVISIBLE);

        if (savedInstanceState != null){
            locationSelectionTracker.onRestoreInstanceState(savedInstanceState);
            locationSelectionAdapter.setSelectionTracker(locationSelectionTracker);
            locationSelectProgressBar.setVisibility(View.INVISIBLE);
            locationSelectProgressMessage.setVisibility(View.INVISIBLE);

            locationSelectRecyclerView.setVisibility(View.VISIBLE);
        }
        else {
            initLocations();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (backgroundWorkExecutor == null) return;

        backgroundWorkExecutor.shutdown();
        try {
            if (!backgroundWorkExecutor.awaitTermination(800, TimeUnit.MILLISECONDS)) {
                backgroundWorkExecutor.shutdownNow();
            }
        }
        catch (InterruptedException ex){
            backgroundWorkExecutor.shutdownNow();
        }
    }

    protected Future<Void> initLocations(){
        try {
            locationSelectProgressBar.setProgress(0);
            locationSelectProgressBar.setVisibility(View.VISIBLE);
            locationSelectProgressMessage.setText(Constants.INFO_LOADING_LOCATIONS);

            return (Future<Void>) backgroundWorkExecutor.submit(() -> {
                try {
                    ServiceFactory.getInstance().getCommunicationService().GetAvailableLocations()
                            .exceptionally(t -> {
                                throw new IllegalStateException(Constants.ERR_FETCHING_LOCATIONS);
                            })
                            .thenAccept(fetchedLocations -> {
                                Handler mainHandler = new Handler(Looper.getMainLooper());
                                mainHandler.post(() -> {
                                    locationSelectProgressBar.setVisibility(View.INVISIBLE);
                                    locationSelectProgressMessage.setVisibility(View.INVISIBLE);

                                    locationSelectionAdapter = new LocationSelectionAdapter(fetchedLocations);
                                    locationSelectRecyclerView.setAdapter(locationSelectionAdapter);
                                    locationSelectRecyclerView.setVisibility(View.VISIBLE);

                                    locationSelectionTracker = new SelectionTracker.Builder<>(
                                            "locationSelectionTrackerId",
                                            locationSelectRecyclerView,
                                            new LocationSelectionKeyProvider(locationSelectionAdapter),
                                            new LocationSelectionLookup(locationSelectRecyclerView),
                                            StorageStrategy.createStringStorage())
                                            .withSelectionPredicate(SelectionPredicates.createSelectSingleAnything())
                                            .build();

                                    locationSelectionAdapter.setSelectionTracker(locationSelectionTracker);
                                    locationSelectionTracker.addObserver(new SelectionTracker.SelectionObserver() {
                                        @Override
                                        public void onItemStateChanged(@NonNull Object key, boolean selected) {
                                            super.onItemStateChanged(key, selected);
                                            int selectedLocationId = Integer.valueOf((String) key).intValue();
                                            Intent resultIntent = new Intent();

                                            int resultCode = Constants.ERROR_SELECTING_LOCATION;
                                            try {
                                                LocationInformation locationInformation = locationSelectionAdapter.getLocationForId(selectedLocationId);
                                                resultCode = Activity.RESULT_OK;
                                            } catch (IllegalArgumentException | NullPointerException ex) {
                                                selectedLocationId = Constants.ERROR_SELECTING_LOCATION;
                                            }
                                            resultIntent.putExtra(Constants.EXTRA_SELECTED_LOCATION_ID, selectedLocationId);
                                            setResult(resultCode, resultIntent);
                                            finish();
                                        }
                                    });
                                });
                            }).get();
                } catch (InterruptedException | ExecutionException e) {
                    Notification.showToast(getApplicationContext(), Constants.ERR_FETCHING_LOCATIONS, true);
                    e.printStackTrace();
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra(Constants.EXTRA_SELECTED_LOCATION_ID, Constants.INVALID_LOCATION_ID);
                    setResult(Constants.ERROR_SELECTING_LOCATION, resultIntent);
                    finish();
                }
            });
        }
        catch (RejectedExecutionException | NullPointerException ex){
            Notification.showToast(getApplicationContext(), Constants.ERR_FETCHING_LOCATIONS, true);
            return CompletableFuture.completedFuture(null);
        }
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        locationSelectionTracker.onSaveInstanceState(outState);
    }
}
