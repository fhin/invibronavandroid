package com.fhin.invibronavapp.activities.location;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.widget.RecyclerView;

import com.fhin.invibronavapp.Constants;
import com.fhin.invibronavapp.R;
import com.fhin.invibronavapp.models.LocationInformation;

import java.util.ArrayList;

public class LocationSelectionAdapter extends RecyclerView.Adapter<LocationSelectionViewHolder>{
    private LocationInformation[] internalLocations;
    private SelectionTracker selectionTracker;

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param data ShopModel[] containing the data to populate views to be used
     * by RecyclerView.
     */
    public LocationSelectionAdapter(Iterable<LocationInformation> data){
        ArrayList<LocationInformation> filteredLocationData = new ArrayList<>();
        for (LocationInformation d : data){
            if (d == null) continue;
            filteredLocationData.add(d);
        }
        internalLocations = new LocationInformation[filteredLocationData.size()];
        internalLocations = filteredLocationData.toArray(internalLocations);
    }

    public void setSelectionTracker(SelectionTracker selectionTracker){
        this.selectionTracker = selectionTracker;
    }

    public LocationInformation getLocationAtPosition(int position){
        if (position >= 0 && position < getItemCount()){
            return internalLocations[position];
        }
        else {
            throw new IndexOutOfBoundsException();
        }
    }

    public LocationInformation getLocationForId(int locationId){
        for (int currIdx = 0; currIdx < getItemCount(); currIdx++){
            if (internalLocations[currIdx].getId() == locationId){
                return internalLocations[currIdx];
            }
        }
        throw new IllegalArgumentException(Constants.BuildMessage(Constants.ERR_NO_LOCATION_FOR_GIVEN_ID, locationId));
    }

    @NonNull
    @Override
    public LocationSelectionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.location_selection_list_item, parent, false);
        return new LocationSelectionViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull LocationSelectionViewHolder holder, int position) {
        LocationInformation newLocationInformation = internalLocations[position];
        holder.Update(newLocationInformation, selectionTracker.isSelected(newLocationInformation));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return internalLocations.length;
    }
}
