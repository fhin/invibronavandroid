package com.fhin.invibronavapp.activities.guiding;

import com.fhin.invibronavapp.Constants;

public class AccelerometerCalibration {
    public enum CalibrationState {
        UNINITIALIZED,
        DONE,
    }

    private static volatile AccelerometerCalibration instance;
    private static final Object LOCK = new Object();

    private int requiredNumberOfMeasurements;
    private volatile int numberOfMeasurements;
    private final float[] gravityForAccelerometer;

    private CalibrationState state;

    private final int numSamplesPerAverage = 5;
    private final float weightingFactor = 1 / (float) numSamplesPerAverage;
    private final float averageWeightingFactor;
    private final float[][] measurementsToWeight;

    public AccelerometerCalibration(int requiredNumberOfMeasurements){
        state = CalibrationState.UNINITIALIZED;
        gravityForAccelerometer = new float[3];
        this.requiredNumberOfMeasurements = requiredNumberOfMeasurements;
        measurementsToWeight = new float[numSamplesPerAverage][];
        averageWeightingFactor = 1 / (float) (requiredNumberOfMeasurements + numSamplesPerAverage - 1);

        for (int i = 0; i < numSamplesPerAverage-1; i++){
            measurementsToWeight[i] = new float[]{0, Constants.DEFAULT_GRAVITY, 0};
        }
    }

    public static AccelerometerCalibration getInstance(int requiredNumberOfMeasurements){
        if (instance == null){
            synchronized (LOCK){
                if (instance == null){
                    if (requiredNumberOfMeasurements < 0){
                        requiredNumberOfMeasurements = Constants.NUM_REQUIRED_ACCELEROMETER_CALIBRATION_MEASUREMENTS;
                    }
                    instance = new AccelerometerCalibration(requiredNumberOfMeasurements);
                }
            }
        }
        return instance;
    }

    public void Reset(){
        instance.numberOfMeasurements = 0;
        instance.gravityForAccelerometer[0] = 0;
        instance.gravityForAccelerometer[1] = Constants.DEFAULT_GRAVITY;
        instance.gravityForAccelerometer[2] = 0;
        state = CalibrationState.UNINITIALIZED;
    }

    // Apply moving average filter to smooth measured signals
    // https://www.mathworks.com/help/signal/ug/signal-smoothing.html;
    public void handleSensorData(float[] sensorData){
        if (isDone()) return;
        numberOfMeasurements++;
        measurementsToWeight[numSamplesPerAverage-1] = sensorData;

        float currDimensionAverage;
        for (int dimensionIdx = 0; dimensionIdx < 3; dimensionIdx++){
            currDimensionAverage = 0;
            for (int measurementIdx = 0; measurementIdx < numSamplesPerAverage; measurementIdx++){
                currDimensionAverage += weightingFactor * measurementsToWeight[measurementIdx][dimensionIdx];
            }
            gravityForAccelerometer[dimensionIdx] += averageWeightingFactor * currDimensionAverage;
        }

        for (int measurementIdx = 0; measurementIdx < numSamplesPerAverage-1; measurementIdx++){
            measurementsToWeight[measurementIdx] = measurementsToWeight[measurementIdx+1];
        }

        if (state == CalibrationState.UNINITIALIZED && numberOfMeasurements == (requiredNumberOfMeasurements+numSamplesPerAverage-1)){
            state = CalibrationState.DONE;
        }
    }

    public boolean isDone(){
        return state == CalibrationState.DONE;
    }

    public float[] getCalibratedValues(){
        return gravityForAccelerometer;
    }
}
