package com.fhin.invibronavapp.activities.guiding;

import android.util.Log;

import com.fhin.invibronavapp.AxisIndex;
import com.fhin.invibronavapp.Constants;

import java.math.BigDecimal;
import java.math.MathContext;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayDeque;
import java.util.Queue;

public class LinearAcceleration {
    public BigDecimal X;
    public BigDecimal Y;
    public BigDecimal Z;

    public BigDecimal accX;
    public BigDecimal accY;
    public BigDecimal accZ;

    private final BigDecimal[] calibratedGravity;
    private BigDecimal[] relativeGravity;
    private Instant lastAccelerationEventTimestamp;
    private BigDecimal durationBetweenAccelerationInSeconds;
    private final BigDecimal[] movAvgVelocity;
    private final Queue<BigDecimal>[] avgVelocityPerAxisQueues;
    private BigDecimal filterLatency;

    public LinearAcceleration(float calibratedGravityX, float calibratedGravityY, float calibratedGravityZ){
        X = BigDecimal.valueOf(0);
        Y = BigDecimal.valueOf(0);
        Z = BigDecimal.valueOf(0);
        calibratedGravity = new BigDecimal[]{
                BigDecimal.valueOf(calibratedGravityX),
                BigDecimal.valueOf(calibratedGravityY),
                BigDecimal.valueOf(calibratedGravityZ)};
        relativeGravity = new BigDecimal[]{
                BigDecimal.valueOf(0),
                BigDecimal.valueOf(0),
                BigDecimal.valueOf(0)
        };
        movAvgVelocity = new BigDecimal[]{
                BigDecimal.valueOf(0),
                BigDecimal.valueOf(0),
                BigDecimal.valueOf(0)
        };
        avgVelocityPerAxisQueues = new Queue[3];
        avgVelocityPerAxisQueues[AxisIndex.X.index] = new ArrayDeque<>(Constants.VELOCITY_WINDOW_SIZE);
        avgVelocityPerAxisQueues[AxisIndex.Y.index] = new ArrayDeque<>(Constants.VELOCITY_WINDOW_SIZE);
        avgVelocityPerAxisQueues[AxisIndex.Z.index] = new ArrayDeque<>(Constants.VELOCITY_WINDOW_SIZE);

        final BigDecimal initialQueueElement = BigDecimal.valueOf(0);
        for (int queueIdx = 0; queueIdx < Constants.VELOCITY_WINDOW_SIZE; queueIdx++){
            avgVelocityPerAxisQueues[AxisIndex.X.index].add(initialQueueElement);
            avgVelocityPerAxisQueues[AxisIndex.Y.index].add(initialQueueElement);
            avgVelocityPerAxisQueues[AxisIndex.Z.index].add(initialQueueElement);
        }

        filterLatency = BigDecimal.valueOf(0);
        lastAccelerationEventTimestamp = Instant.now();
        durationBetweenAccelerationInSeconds = BigDecimal.valueOf(0);
        accX = BigDecimal.valueOf(0);
        accY = BigDecimal.valueOf(0);
        accZ = BigDecimal.valueOf(0);
    }

    public void updateCalibratedGravity(float calibratedGravityX, float calibratedGravityY, float calibratedGravityZ){
        calibratedGravity[AxisIndex.X.index] = BigDecimal.valueOf(calibratedGravityX);
        calibratedGravity[AxisIndex.Y.index] = BigDecimal.valueOf(calibratedGravityY);
        calibratedGravity[AxisIndex.Z.index] = BigDecimal.valueOf(calibratedGravityZ);
    }

    // Perform a basic filter to subtract the force of gravity from the given raw acceleration sensor data
    // see: https://developer.android.com/guide/topics/sensors/sensors_motion#sensors-motion-linear
    // further reading:
    // https://dsp.stackexchange.com/questions/3026/picking-the-correct-filter-for-accelerometer-data
    // https://nitinjsanket.github.io/tutorials/attitudeest/mahony
    //
    // TODO: Important readme
    // https://developer.download.nvidia.com/tegra/docs/tegra_android_accelerometer_v5f.pdf
    // https://stackoverflow.com/questions/35677379/android-rotation-matrix-for-accelerometer-transformation
    // https://android-developers.googleblog.com/2010/09/one-screen-turn-deserves-another.html
    public void transformSensorData(final float[] rotationMatrix, final float[] I, float rawSensorXAcceleration, float rawSensorYAcceleration, float rawSensorZAcceleration){
        final Instant newEventTimestamp = Instant.now();

        BigDecimal timeElapsedSinceLastEvent = BigDecimal.valueOf(Duration.between(lastAccelerationEventTimestamp, newEventTimestamp).toMillis());
        timeElapsedSinceLastEvent = timeElapsedSinceLastEvent.divide(BigDecimal.valueOf(1000));
        lastAccelerationEventTimestamp = newEventTimestamp;

        // alpha = filterDelay / (filterDelay + eventDeliveryRate)
        final BigDecimal alpha = filterLatency.divide(filterLatency.add(timeElapsedSinceLastEvent), MathContext.DECIMAL128);
       // final BigDecimal alpha = BigDecimal.valueOf(0.85);
        final Instant filterLatencyStart = Instant.now();


        relativeGravity[AxisIndex.X.index] = alpha
                .multiply(calibratedGravity[AxisIndex.X.index])
                .add(alpha
                        .subtract(BigDecimal.valueOf(1))
                        .multiply(BigDecimal.valueOf(rawSensorXAcceleration)));

        relativeGravity[AxisIndex.Y.index] = alpha
                .multiply(calibratedGravity[AxisIndex.Y.index])
                .add(alpha
                        .subtract(BigDecimal.valueOf(1))
                        .multiply(BigDecimal.valueOf(rawSensorYAcceleration)));

        relativeGravity[AxisIndex.Z.index] = alpha
                .multiply(calibratedGravity[AxisIndex.Z.index])
                .add(alpha
                        .subtract(BigDecimal.valueOf(1))
                        .multiply(BigDecimal.valueOf(rawSensorZAcceleration)));
        /*

        relativeGravity[AxisIndex.X.index] = calibratedGravity[AxisIndex.X.index];
        relativeGravity[AxisIndex.Y.index] = calibratedGravity[AxisIndex.Y.index];
        relativeGravity[AxisIndex.Z.index] = calibratedGravity[AxisIndex.Z.index];

         */
        final BigDecimal sensorXAcceleration = BigDecimal.valueOf(rawSensorXAcceleration).subtract(relativeGravity[AxisIndex.X.index]);
        final BigDecimal sensorYAcceleration = BigDecimal.valueOf(rawSensorYAcceleration).subtract(relativeGravity[AxisIndex.Y.index]);
        final BigDecimal sensorZAcceleration = BigDecimal.valueOf(rawSensorZAcceleration).subtract(relativeGravity[AxisIndex.Z.index]);

        final float[] deviceLocalAcceleration = new float[4];
        deviceLocalAcceleration[0] = sensorXAcceleration.floatValue();
        deviceLocalAcceleration[1] = sensorYAcceleration.floatValue();
        deviceLocalAcceleration[2] = sensorZAcceleration.floatValue();
        deviceLocalAcceleration[3] = 0;

        final float[] invertedRotationMatrix = new float[16];
        final float[] globalAcceleration = new float[16];
        android.opengl.Matrix.invertM(invertedRotationMatrix, 0, rotationMatrix, 0);
        android.opengl.Matrix.multiplyMV(globalAcceleration, 0, I, 0, deviceLocalAcceleration, 0);

        final BigDecimal globalXAcceleration = BigDecimal.valueOf(globalAcceleration[AxisIndex.X.index]);
        final BigDecimal globalYAcceleration = BigDecimal.valueOf(globalAcceleration[AxisIndex.Y.index]);
        final BigDecimal globalZAcceleration = BigDecimal.valueOf(globalAcceleration[AxisIndex.Z.index]);

        Log.d("ACCELERATION", String.format("Curr. Acceleration: X: %f, Y: %f, Z: %f", new Object[]{globalXAcceleration.floatValue(), globalYAcceleration.floatValue(), globalZAcceleration.floatValue()}));

        updateVelocity(AxisIndex.X.index, globalXAcceleration, timeElapsedSinceLastEvent);
        updateVelocity(AxisIndex.Y.index, globalYAcceleration, timeElapsedSinceLastEvent);
        updateVelocity(AxisIndex.Z.index, globalZAcceleration, timeElapsedSinceLastEvent);

        X = movAvgVelocity[AxisIndex.X.index].multiply(timeElapsedSinceLastEvent);
        Y = movAvgVelocity[AxisIndex.Y.index].multiply(timeElapsedSinceLastEvent);
        Z = movAvgVelocity[AxisIndex.Z.index].multiply(timeElapsedSinceLastEvent);

        //calculateCurrentMovement(timeElapsedSinceLastEvent, sensorXAcceleration, sensorYAcceleration, sensorZAcceleration);

        final Instant filterLatencyEnd = Instant.now();
        filterLatency = BigDecimal.valueOf(Duration.between(filterLatencyStart, filterLatencyEnd).toMillis());
    }

    // https://en.wikipedia.org/wiki/Semi-implicit_Euler_method
    private void updateVelocity(int axisIdx, final BigDecimal acceleration, final BigDecimal time){
        BigDecimal currVelocity = BigDecimal.valueOf(1E-12f);
        if (time.compareTo(BigDecimal.valueOf(0.0)) > 0){
            // v_n+1 = a_
            currVelocity = currVelocity.add(acceleration.multiply(time));
        }
        movAvgVelocity[axisIdx] = currVelocity;

        // https://en.wikipedia.org/wiki/Moving_average
        if (axisIdx == 0){
            Log.d("ACCELERATION", String.format("Avg velocity (Axis: %d): %f", new Object[]{axisIdx, movAvgVelocity[axisIdx].floatValue()}));
        }
    }
}
