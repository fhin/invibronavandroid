package com.fhin.invibronavapp.activities.shop;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.selection.SelectionPredicates;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.selection.StorageStrategy;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.fhin.invibronavapp.Constants;
import com.fhin.invibronavapp.R;
import com.fhin.invibronavapp.activities.Notification;
import com.fhin.invibronavapp.backendcommunication.EntityCacheService;
import com.fhin.invibronavapp.backendcommunication.ServiceFactory;
import com.fhin.invibronavapp.models.ShopInformation;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

public class ShopSelectionActivity extends AppCompatActivity {

    private TextView shopSelectionDescription;
    private ImageView locationIcon;
    private TextView locationName;
    private EditText shopNameSearchField;
    private TextView shopSelectionProgressMessage;
    private ProgressBar shopSelectionProgressBar;

    private RecyclerView shopList;
    private ShopSelectionAdapter shopSelectionAdapter;
    private SelectionTracker shopSelectionTracker;

    private ExecutorService backgroundWorkExecutor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_selection);

        backgroundWorkExecutor = Executors.newFixedThreadPool(1);

        shopSelectionDescription = findViewById(R.id.shopSelectionDescriptionText);
        shopSelectionDescription.setText(Constants.INFO_HEADER_SHOP_SELECTION);

        locationIcon = findViewById(R.id.shopSelectionLocationIcon);
        locationName = findViewById(R.id.shopSelectionLocationName);
        shopNameSearchField = findViewById(R.id.shopSelectionNameSearchField);

        shopSelectionProgressMessage = findViewById(R.id.shopSelectionProgressMessage);
        shopSelectionProgressBar = findViewById(R.id.shopSelectionProgressBar);

        shopList = findViewById(R.id.shopSelectionRecyclerView);
        shopList.setLayoutManager(new LinearLayoutManager(this));
        shopList.setVisibility(View.INVISIBLE);

        Intent shopSelectionIntent = getIntent();
        int selectedLocationId = shopSelectionIntent.getIntExtra(Constants.EXTRA_SELECTED_LOCATION_ID, Constants.INVALID_LOCATION_ID);
        String selectedLocationName = EntityCacheService.getInstance().getCachedLocation(selectedLocationId).getName();
        locationName.setText(selectedLocationName);

        initShopInformation(selectedLocationId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (backgroundWorkExecutor == null) return;

        backgroundWorkExecutor.shutdown();
        try {
            if (!backgroundWorkExecutor.awaitTermination(800, TimeUnit.MILLISECONDS)) {
                backgroundWorkExecutor.shutdownNow();
            }
        }
        catch (InterruptedException ex){
            backgroundWorkExecutor.shutdownNow();
        }
    }

    protected Future<Void> initShopInformation(int locationId){
        try {
            shopSelectionProgressBar.setVisibility(View.VISIBLE);
            shopSelectionProgressBar.setProgress(0);
            shopSelectionProgressMessage.setText(Constants.BuildMessage(Constants.INFO_LOADING_SHOPS_FOR_LOCATION, locationName.getText()));
            shopNameSearchField.setText("LOADING...");
            shopNameSearchField.setInputType(InputType.TYPE_NULL);

            return (Future<Void>) backgroundWorkExecutor.submit(() -> {
                try {
                    ServiceFactory.getInstance().getCommunicationService().GetAvailableShopsForLocation(locationId)
                    .exceptionally(t -> {
                        throw new IllegalStateException(Constants.BuildMessage(Constants.ERR_FETCHING_SHOPS_FOR_LOCATION, locationId));
                    })
                    .thenAccept(fetchedShopsForLocation -> {
                        Handler mainHandler = new Handler(Looper.getMainLooper());
                        mainHandler.post(() -> {
                            shopSelectionProgressMessage.setVisibility(View.INVISIBLE);
                            shopSelectionProgressBar.setVisibility(View.INVISIBLE);

                            shopSelectionAdapter = new ShopSelectionAdapter(fetchedShopsForLocation);
                            shopList.setAdapter(shopSelectionAdapter);
                            shopList.setVisibility(View.VISIBLE);

                            shopSelectionTracker = new SelectionTracker.Builder<>(
                                    "shopForLocationSelectionTrackerId",
                                    shopList,
                                    new ShopSelectionKeyProvider(shopSelectionAdapter),
                                    new ShopSelectionLookup(shopList),
                                    StorageStrategy.createStringStorage())
                                    .withSelectionPredicate(SelectionPredicates.createSelectSingleAnything())
                                    .build();
                            shopSelectionAdapter.setSelectionTracker(shopSelectionTracker);

                            shopSelectionTracker.addObserver(new SelectionTracker.SelectionObserver() {
                                @Override
                                public void onItemStateChanged(@NonNull Object key, boolean selected) {
                                    super.onItemStateChanged(key, selected);
                                    int selectedShopId = Integer.valueOf((String) key).intValue();

                                    int resultCode = Activity.RESULT_OK;
                                    Intent resultIntent = new Intent();
                                    try {
                                        ShopInformation selectedShopForId = shopSelectionAdapter.findShopForId(selectedShopId);
                                        if (selectedShopForId == null) {
                                            selectedShopId = Constants.INVALID_SHOP_ID;
                                            resultCode = Constants.INVALID_SHOP_ID;
                                        }
                                    } catch (IllegalArgumentException ex) {
                                        selectedShopId = Constants.INVALID_SHOP_ID;
                                        resultCode = Constants.ERROR_SELECTING_SHOP;
                                    }
                                    resultIntent.putExtra(Constants.EXTRA_SELECTED_LOCATION_ID, locationId);
                                    resultIntent.putExtra(Constants.EXTRA_SELECTED_SHOP_ID, selectedShopId);
                                    setResult(resultCode, resultIntent);
                                    finish();
                                }
                            });
                            shopNameSearchField.setText("Enter a shop name");
                            shopNameSearchField.setInputType(InputType.TYPE_CLASS_TEXT);
                        });
                    })
                    .get();
                }
                catch (InterruptedException | ExecutionException e) {
                    Notification.showToast(getApplicationContext(), Constants.ERR_FETCHING_LOCATIONS, true);
                    e.printStackTrace();

                    Intent resultIntent = new Intent();
                    resultIntent.putExtra(Constants.EXTRA_SELECTED_LOCATION_ID, locationId);
                    resultIntent.putExtra(Constants.EXTRA_SELECTED_SHOP_ID, Constants.INVALID_SHOP_ID);
                    setResult(Constants.ERROR_SELECTING_SHOP, resultIntent);
                    finish();
                }
            });
        }
        catch (RejectedExecutionException | NullPointerException ex){
            String errMsg = Constants.BuildMessage(Constants.ERR_FETCHING_SHOPS_FOR_LOCATION, locationId);
            Notification.showToast(getApplicationContext(), errMsg, true);

            Intent resultIntent = new Intent();
            resultIntent.putExtra(Constants.EXTRA_SELECTED_LOCATION_ID, locationId);
            resultIntent.putExtra(Constants.EXTRA_SELECTED_SHOP_ID, Constants.INVALID_SHOP_ID);
            setResult(Constants.ERROR_SELECTING_SHOP, resultIntent);
            finish();

            return CompletableFuture.completedFuture(null);
        }
    }
}