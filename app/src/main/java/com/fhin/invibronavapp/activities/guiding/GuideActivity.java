package com.fhin.invibronavapp.activities.guiding;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.fhin.invibronavapp.Constants;
import com.fhin.invibronavapp.R;
import com.fhin.invibronavapp.activities.Notification;
import com.fhin.invibronavapp.backendcommunication.EntityCacheService;
import com.fhin.invibronavapp.backendcommunication.ServiceFactory;
import com.fhin.invibronavapp.models.Direction;
import com.fhin.invibronavapp.models.DirectionChange;
import com.fhin.invibronavapp.models.LocationInformation;
import com.fhin.invibronavapp.models.ShopInformation;
import com.fhin.invibronavapp.models.Waypoint;
import com.fhin.invibronavapp.services.GuidingService;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class GuideActivity extends AppCompatActivity {
    private TextView currentTargetName;
    private ImageView currentTargetIcon;

    private TextView guidingLoadingProgressMessage;
    private ProgressBar guidingLoadingProgressBar;

    private ImageView directionIconNorth;
    private ImageView directionIconNorthEast;
    private ImageView directionIconEast;
    private ImageView directionIconSouthEast;
    private ImageView directionIconSouth;
    private ImageView directionIconSouthWest;
    private ImageView directionIconWest;
    private ImageView directionIconNorthWest;

    private ExecutorService backgroundWorkExecutor;
    private GuidingService guidingService;
    private GuideViewModel guideViewModel;

    private Direction currentTargetDirection;
    private Direction currentDeviceDirection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivity_guiding);

        backgroundWorkExecutor = Executors.newFixedThreadPool(1);
        currentTargetDirection = Direction.INVALID;
        currentDeviceDirection = Direction.INVALID;

        guidingService = null;

        currentTargetName = findViewById(R.id.guidingTargetName);
        currentTargetIcon = findViewById(R.id.guidingTargetIcon);

        guidingLoadingProgressMessage = findViewById(R.id.guidingLoadingProgressMessage);
        guidingLoadingProgressBar = findViewById(R.id.guidingLoadingProgressBar);

        directionIconNorth = findViewById(R.id.guidingDirectionElemNorth);
        directionIconNorthEast = findViewById(R.id.guidingDirectionElemNorthEast);
        directionIconEast = findViewById(R.id.guidingDirectionElemEast);
        directionIconSouthEast = findViewById(R.id.guidingDirectionElemSouthEast);
        directionIconSouth = findViewById(R.id.guidingDirectionElemSouth);
        directionIconSouthWest = findViewById(R.id.guidingDirectionElemSouthWest);
        directionIconWest = findViewById(R.id.guidingDirectionElemWest);
        directionIconNorthWest = findViewById(R.id.guidingDirectionElemNorthWest);

        Intent guidingStartIntent = getIntent();
        int selectedShopId = guidingStartIntent.getIntExtra(Constants.EXTRA_SELECTED_SHOP_ID, Constants.INVALID_SHOP_ID);
        int selectedLocationId = guidingStartIntent.getIntExtra(Constants.EXTRA_SELECTED_LOCATION_ID, Constants.INVALID_LOCATION_ID);

        LocationInformation selectLocationInfo = EntityCacheService.getInstance().getCachedLocation(selectedLocationId);
        ShopInformation selectedShopInfo = EntityCacheService.getInstance().getCachedShopInformation(selectedLocationId, selectedShopId);
        if (selectLocationInfo == null || selectedShopInfo == null){
            finish();
        }

        currentTargetName.setText(selectedShopInfo.getName());
        currentTargetIcon.setBackgroundColor(Color.GREEN);

        guideViewModel = new ViewModelProvider(this).get(GuideViewModel.class);
        guideViewModel.getDeviceDirection().observe(this, new Observer<DirectionChange>() {
            @Override
            public void onChanged(DirectionChange directionChange) {
                handleDirectionUpdate(directionChange);
            }
        });
        guideViewModel.getTargetDirection().observe(this, new Observer<DirectionChange>() {
            @Override
            public void onChanged(DirectionChange directionChange) {
                handleDirectionUpdate(directionChange);
            }
        });
        initGuiding(0, 0, selectLocationInfo.getId(), selectedShopInfo.getId(), selectedShopInfo.getName(), selectLocationInfo.getName());
    }

    @Override
    protected void onPause(){
        super.onPause();
        if (guidingService != null){
            guidingService.pause();
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        if (guidingService != null){
            guidingService.resume();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (backgroundWorkExecutor == null) return;

        getApplicationContext().stopService(new Intent(this, GuidingService.class));
        /*
        if (guidingService != null){
            guidingService.pause();
            guidingService.stopForeground(true);
        }
        */

        backgroundWorkExecutor.shutdown();
        try {
            if (!backgroundWorkExecutor.awaitTermination(800, TimeUnit.MILLISECONDS)) {
                backgroundWorkExecutor.shutdownNow();
            }
        }
        catch (InterruptedException ex){
            backgroundWorkExecutor.shutdownNow();
        }
    }

    // TODO: Add handling to save and restore current state (i.e. current path and relative distance to current target)
    // https://developer.android.com/guide/components/activities/activity-lifecycle

    private Future<Void> initGuiding(double locationX, double locationY, int selectedLocationId, int selectedShopId, String shopName, String locationName){
        try {
            if (selectedLocationId == Constants.INVALID_LOCATION_ID || selectedShopId == Constants.INVALID_SHOP_ID){
                String errMsg = Constants.BuildMessage(Constants.ERR_LOADING_PATH, new Object[]{shopName, locationName});
                Notification.showToast(getApplicationContext(), errMsg, true);
                finish();
            }
            guidingLoadingProgressBar.setProgress(0);
            guidingLoadingProgressBar.setVisibility(View.VISIBLE);
            guidingLoadingProgressMessage.setText(Constants.INFO_INIT_GUIDING);
            greyOutAllDirections();

            Handler mainHandler = new Handler(Looper.getMainLooper());

            return (Future<Void>) backgroundWorkExecutor.submit(() -> {
                try {
                    ServiceFactory.getInstance().getCommunicationService().GetPath(0, 0, selectedLocationId, selectedShopId)
                            .exceptionally(t -> {
                                mainHandler.post(() -> {
                                    finish();
                                });
                                return null;
                            })
                            .thenAccept(loadedWaypoints -> {
                                final ArrayList<Waypoint> pathToTarget = new ArrayList<>();
                                for (Waypoint fetchedWaypoint : loadedWaypoints){
                                    if (fetchedWaypoint == null) continue;
                                    pathToTarget.add(fetchedWaypoint);
                                }

                                mainHandler.post(() -> {
                                    guidingLoadingProgressMessage.setText(Constants.INFO_GUIDING_CALIBRATION_MSG);
                                    Intent guidingServiceStartIntent = new Intent(this, GuidingService.class);
                                    guidingServiceStartIntent.putExtra(Constants.EXTRA_GUIDING_LOCATION_TARGET_NAME, shopName);
                                    guidingServiceStartIntent.putParcelableArrayListExtra(Constants.EXTRA_WAYPOINTS_TO_TARGET, pathToTarget);
                                    if (getApplicationContext().startForegroundService(guidingServiceStartIntent) != null) {
                                        //guideViewModel.getDeviceDirection().observe(this, directionChange -> handleDirectionUpdate(directionChange));
                                        //guideViewModel.getTargetDirection().observe(this, directionChange -> handleDirectionUpdate(directionChange));
                                    }
                                });
                            })
                            .thenRunAsync(() -> {
                                AccelerometerCalibration instance = AccelerometerCalibration.getInstance(Constants.NUM_REQUIRED_ACCELEROMETER_CALIBRATION_MEASUREMENTS);
                                while (!instance.isDone()){
                                    try {
                                        Thread.sleep(500);
                                    }
                                    catch (InterruptedException ex){}
                                }
                                mainHandler.post(() -> {
                                    guidingLoadingProgressMessage.setVisibility(View.INVISIBLE);
                                    guidingLoadingProgressBar.setVisibility(View.INVISIBLE);
                                    //applyOrRemoveColorFilter(currDirection, false);
                                });
                            })
                            .exceptionally(t -> {
                                mainHandler.post(() -> {
                                    finish();
                                });
                                return null;
                            })
                            .get();
                }
                catch (InterruptedException | ExecutionException e) {
                    String errMsg = Constants.BuildMessage(Constants.ERR_LOADING_PATH, new Object[]{shopName, locationName});
                    Notification.showToast(getApplicationContext(), errMsg, true);
                    e.printStackTrace();
                }
            });
        }
        catch (RejectedExecutionException | NullPointerException ex){
            String errMsg = Constants.BuildMessage(Constants.ERR_LOADING_PATH, new Object[]{shopName, locationName});
            Notification.showToast(getApplicationContext(), errMsg, true);
            return CompletableFuture.completedFuture(null);
        }
    }

    private synchronized void handleDirectionUpdate(DirectionChange directionChange){
        if (directionChange == null){
            return;
        }

        Direction previousDirection = directionChange.previousDirection;
        if (directionChange.forDevice){
            currentDeviceDirection = directionChange.newDirection;
            if (currentDeviceDirection == Direction.INVALID){
                Notification.showToast(this, "DESTINATION REACHED", false);
                finish();
                return;
            }
        }
        else {
            currentTargetDirection = directionChange.newDirection;
        }

        if (currentDeviceDirection == currentTargetDirection){
            applyOrRemoveColorFilter(previousDirection, true);
            applyDeviceDirectionColorFilter(currentDeviceDirection, 75, 0, 130);
            return;
        }

        Direction otherDirection = !directionChange.forDevice ? currentDeviceDirection : currentTargetDirection;
        // Change broke shared state, reset other state to its unique color
        if (previousDirection == otherDirection){
            if (directionChange.forDevice){
                applyOrRemoveColorFilter(otherDirection, false);
            }
            else {
                applyDeviceDirectionColorFilter(otherDirection, 255,0 ,0);
            }
        }
        else {
            applyOrRemoveColorFilter(previousDirection, true);
        }

        if (directionChange.forDevice){
            applyDeviceDirectionColorFilter(currentDeviceDirection, 255, 0, 0);
        }
        else {
            applyOrRemoveColorFilter(currentTargetDirection, false);
        }
    }

    private void greyOutAllDirections(){
        applyOrRemoveColorFilter(Direction.NORTH, true);
        applyOrRemoveColorFilter(Direction.NORTH_EAST, true);
        applyOrRemoveColorFilter(Direction.EAST, true);
        applyOrRemoveColorFilter(Direction.SOUTH_EAST, true);
        applyOrRemoveColorFilter(Direction.SOUTH, true);
        applyOrRemoveColorFilter(Direction.SOUTH_WEST, true);
        applyOrRemoveColorFilter(Direction.WEST, true);
        applyOrRemoveColorFilter(Direction.NORTH_WEST, true);
    }

    private void applyOrRemoveColorFilter(Direction direction, boolean applyGrayscaleFilter){
        ImageView targetImageView;
        switch (direction){
            case NORTH:
                targetImageView = directionIconNorth;
                break;
            case NORTH_EAST:
                targetImageView = directionIconNorthEast;
                break;
            case EAST:
                targetImageView = directionIconEast;
                break;
            case SOUTH_EAST:
                targetImageView = directionIconSouthEast;
                break;
            case SOUTH:
                targetImageView = directionIconSouth;
                break;
            case SOUTH_WEST:
                targetImageView = directionIconSouthWest;
                break;
            case WEST:
                targetImageView = directionIconWest;
                break;
            case NORTH_WEST:
                targetImageView = directionIconNorthWest;
                break;
            default:
                // TODO: logging
                return;
        }

        if (applyGrayscaleFilter){
            targetImageView.setColorFilter(Color.argb(200, 200, 200, 200));
        }
        else {
            targetImageView.clearColorFilter();
            targetImageView.setImageAlpha(255);
        }
    }

    private void applyDeviceDirectionColorFilter(Direction direction, int red, int green, int blue){
        ImageView targetImageView;
        switch (direction){
            case NORTH:
                targetImageView = directionIconNorth;
                break;
            case NORTH_EAST:
                targetImageView = directionIconNorthEast;
                break;
            case EAST:
                targetImageView = directionIconEast;
                break;
            case SOUTH_EAST:
                targetImageView = directionIconSouthEast;
                break;
            case SOUTH:
                targetImageView = directionIconSouth;
                break;
            case SOUTH_WEST:
                targetImageView = directionIconSouthWest;
                break;
            case WEST:
                targetImageView = directionIconWest;
                break;
            case NORTH_WEST:
                targetImageView = directionIconNorthWest;
                break;
            default:
                // TODO: logging
                return;
        }
        targetImageView.setColorFilter(Color.argb(200, red,green,blue));
    }
}
