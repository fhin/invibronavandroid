package com.fhin.invibronavapp.activities.location;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.selection.ItemKeyProvider;

import com.fhin.invibronavapp.Constants;
import com.fhin.invibronavapp.models.LocationInformation;

public class LocationSelectionKeyProvider extends ItemKeyProvider<String> {
    private LocationSelectionAdapter adapter;

    public LocationSelectionKeyProvider(LocationSelectionAdapter adapter)
    {
        super(ItemKeyProvider.SCOPE_CACHED);
        this.adapter = adapter;
    }

    @Nullable
    @Override
    public String getKey(int position) {
        try {
            LocationInformation lInfo = adapter.getLocationAtPosition(position);
            return String.valueOf(lInfo.getId());
        }
        catch (IndexOutOfBoundsException|NullPointerException ex){
            return null;
        }
    }

    @Override
    public int getPosition(@NonNull String key) {
        if (key == null) return Constants.NO_SELECTED_LIST_ITEM_IDX;

        for (int currIdx = 0; currIdx < adapter.getItemCount(); currIdx++) {
            String keyForItemAtIndex = getKey(currIdx);
            if (keyForItemAtIndex == null) continue;
            if (keyForItemAtIndex.equals(key)){
                return currIdx;
            }
        }
        return Constants.NO_SELECTED_LIST_ITEM_IDX;
    }
}
