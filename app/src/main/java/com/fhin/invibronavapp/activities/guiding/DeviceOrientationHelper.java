package com.fhin.invibronavapp.activities.guiding;

import android.hardware.SensorManager;

import com.fhin.invibronavapp.Constants;

// Based on the source code found @
// https://developer.android.com/guide/topics/sensors/sensors_position#sensors-pos-orient
public class DeviceOrientationHelper {
    public final float[] accelerometerReading;
    public final float[] magnetometerReading;

    private final float[] rotationMatrix;
    private final float[] orientationAngles;

    private int azimuthAverageWindowSize;
    private float[] azimuthInRadHistory;

    public DeviceOrientationHelper(){
        accelerometerReading = new float[3];
        magnetometerReading = new float[3];
        rotationMatrix = new float[9];
        orientationAngles = new float[3];

        azimuthAverageWindowSize = Constants.AZIMUTH_HISTORY_LENGTH;
        azimuthInRadHistory = new float[azimuthAverageWindowSize];
    }

    public void UpdateAccelerometerReadings(float[] accelerometerReadings){
        System.arraycopy(accelerometerReadings, 0, this.accelerometerReading, 0, accelerometerReading.length);
        ReorientWithNewValues();
    }

    public void UpdateMagnetometerReadings(float[] magnetometerReadings){
        System.arraycopy(magnetometerReadings, 0, this.magnetometerReading, 0, magnetometerReading.length);
        ReorientWithNewValues();
    }

    private void ReorientWithNewValues(){
        SensorManager.getRotationMatrix(rotationMatrix, null, accelerometerReading, magnetometerReading);
        SensorManager.getOrientation(rotationMatrix, orientationAngles);

        float transformedAzimuth = TransformRawAzimuth(orientationAngles[0]);
        System.arraycopy(azimuthInRadHistory, 1, azimuthInRadHistory, 0, azimuthAverageWindowSize-1);
        azimuthInRadHistory[azimuthAverageWindowSize-1] = transformedAzimuth;
    }

    private float TransformRawAzimuth(float rawAzimuth){
        float azimuthInRadiant = rawAzimuth;
        // Transform azimuth so heading EAST equals an Azimuth of 0 rad
        azimuthInRadiant -= Math.PI / 2;
        if (azimuthInRadiant < 0){
            azimuthInRadiant *= -1;
        }
        return azimuthInRadiant;
    }

    /**
     * This is the angle between the device's current compass direction and magnetic north (rotation around the -z axis)
     * @return The rotation around the y axis in degrees [0 = east, north = pi/2, west = pi, south=3pi/2]
     */
    public double GetAzimuth(){
        double avgAzimuth = 0;
       for (int azimuthHistoryIndex = 0; azimuthHistoryIndex < azimuthAverageWindowSize; azimuthHistoryIndex++){
           avgAzimuth += azimuthInRadHistory[azimuthHistoryIndex];
       }
       return Math.toDegrees(avgAzimuth / azimuthAverageWindowSize);
    }

    /**
     * Get the angle between a plane parallel to the device's screen and a plane parallel to the ground (rotation around the x axis).
     * @return The rotation around the y axis in degrees
     */
    public double GetRoll(){
        return Math.toDegrees(orientationAngles[1]);
    }

    /**
     * Get the angle between a plane perpendicular to the device's screen and a plane perpendicular to the ground (rotation around the y axis).
     * @return The rotation around the y axis in degrees
     */
    public double GetPitch(){
        return Math.toDegrees(orientationAngles[2]);
    }
}
