package com.fhin.invibronavapp.activities.main;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.appcompat.app.AppCompatActivity;

import com.fhin.invibronavapp.Constants;
import com.fhin.invibronavapp.R;
import com.fhin.invibronavapp.activities.Notification;
import com.fhin.invibronavapp.activities.guiding.GuideActivity;
import com.fhin.invibronavapp.backendcommunication.ServiceFactory;
import com.fhin.invibronavapp.contracts.PickLocationContract;
import com.fhin.invibronavapp.contracts.PickShopContract;
import com.fhin.invibronavapp.contracts.ShopSelectionResult;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {
    private TextView mainAppName;
    private ImageView mainAppLogo;
    private TextView mainProgressStatusMessage;
    private ProgressBar mainProgressBar;
    private Button mainButton;

    private ExecutorService backgroundWorkExecutor;

    private final ActivityResultLauncher<Void> selectLocationActivity = registerForActivityResult(new PickLocationContract(),
            new ActivityResultCallback<Integer>() {
                @Override
                public void onActivityResult(Integer selectedLocationResult) {
                    int selectedLocationId = selectedLocationResult.intValue();
                    if (selectedLocationId != Constants.INVALID_LOCATION_ID && selectedLocationId != Constants.ERROR_SELECTING_LOCATION){
                        selectShopActivity.launch(selectedLocationResult);
                    }
                    else {
                        String errMsg = selectedLocationId == Constants.INVALID_LOCATION_ID ? Constants.ERR_INVALID_LOCATION_SELECTED : Constants.ERR_WHILE_SELECTING_LOCATION;
                        Notification.showToast(getApplicationContext(), errMsg, true);
                        mainButton.setText(Constants.INFO_PICK_LOCATION);
                        mainButton.setOnClickListener(v -> {
                            LoadAvailableLocations();
                        });
                    }
                }
            });

    private final ActivityResultLauncher<Integer> selectShopActivity = registerForActivityResult(new PickShopContract(),
            new ActivityResultCallback<ShopSelectionResult>() {
                @Override
                public void onActivityResult(ShopSelectionResult result) {
                    int selectedShopId = result.shopId;
                    if (selectedShopId == Constants.INVALID_SHOP_ID){
                        Toast errToast = Toast.makeText(getApplicationContext(), Constants.ERR_INVALID_SHOP_SELECTED, Toast.LENGTH_SHORT);
                        errToast.show();
                    }
                    else if (selectedShopId == Constants.ERROR_SELECTING_SHOP){
                        Toast errToast = Toast.makeText(getApplicationContext(), Constants.ERR_WHILE_SELECTING_SHOP, Toast.LENGTH_SHORT);
                        errToast.show();
                    }
                    else {
                        Toast succToast = Toast.makeText(getApplicationContext(), "LId: " + String.valueOf(result.locationId) + " | SID: " + String.valueOf(result.shopId), Toast.LENGTH_SHORT);
                        succToast.show();
                        Intent guidingStartIntent = new Intent(getBaseContext(), GuideActivity.class);
                        guidingStartIntent.putExtra(Constants.EXTRA_SELECTED_LOCATION_ID, result.locationId);
                        guidingStartIntent.putExtra(Constants.EXTRA_SELECTED_SHOP_ID, result.shopId);
                        startActivity(guidingStartIntent);
                    }
                }
            });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        backgroundWorkExecutor = Executors.newFixedThreadPool(1);
        setContentView(R.layout.activity_main);

        mainAppName = findViewById(R.id.mainAppName);
        mainAppLogo = findViewById(R.id.mainAppLogo);
        mainProgressStatusMessage = findViewById(R.id.mainProgressMessage);
        mainProgressBar = findViewById(R.id.mainProgressBar);
        mainButton = findViewById(R.id.mainLaunchButton);
        mainButton.setVisibility(View.INVISIBLE);

        LoadAvailableLocations();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (backgroundWorkExecutor == null) return;

        backgroundWorkExecutor.shutdown();
        try {
            if (!backgroundWorkExecutor.awaitTermination(800, TimeUnit.MILLISECONDS)) {
                backgroundWorkExecutor.shutdownNow();
            }
        }
        catch (InterruptedException ex){
            backgroundWorkExecutor.shutdownNow();
        }
    }

    public void startLocationSelection(View view) {
        // Do something in response to button click
        selectLocationActivity.launch(null);
    }

    private Future<Void> LoadAvailableLocations(){
        try {
            // Make no action available
            mainButton.setVisibility(View.INVISIBLE);
            // Reset progress bar
            mainProgressBar.setProgress(0);
            mainProgressBar.setVisibility(View.VISIBLE);
            // Set appropriate status message
            mainProgressStatusMessage.setText(Constants.INFO_LOADING_LOCATIONS);

            Handler mainHandler = new Handler(Looper.getMainLooper());

            return (Future<Void>) backgroundWorkExecutor.submit(() -> {
                try {
                    ServiceFactory.getInstance().getCommunicationService().GetAvailableLocations()
                            .exceptionally(t -> {
                                Notification.showToast(getApplicationContext(), Constants.ERR_FETCHING_LOCATIONS, true);
                                mainHandler.post(() -> {
                                    mainButton.setText(Constants.INFO_TRIGGER_MANUAL_LOCATION_FETCH);
                                });
                                return null;
                            })
                            .thenAccept(fetchedLocations -> {
                                mainHandler.post(() -> {
                                    mainProgressStatusMessage.setText("");
                                    mainProgressBar.setVisibility(View.INVISIBLE);
                                    mainButton.setText(Constants.INFO_PICK_LOCATION);
                                    mainButton.setOnClickListener(v -> {
                                        // your handler code here
                                        startLocationSelection(v);
                                    });
                                    mainButton.setVisibility(View.VISIBLE);
                                });
                            }).get();
                } catch (InterruptedException | ExecutionException e) {
                    Notification.showToast(getApplicationContext(), Constants.ERR_FETCHING_LOCATIONS, true);
                    e.printStackTrace();
                }
            });
        }
        catch (RejectedExecutionException | NullPointerException ex){
            Notification.showToast(getApplicationContext(), Constants.ERR_FETCHING_LOCATIONS, true);
            return CompletableFuture.completedFuture(null);
        }
    }
}