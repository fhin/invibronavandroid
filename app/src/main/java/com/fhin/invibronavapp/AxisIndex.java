package com.fhin.invibronavapp;

public enum AxisIndex {
    X(0),
    Y(1),
    Z(2);

    public final int index;

    AxisIndex(int index){
        this.index = index;
    }
}
