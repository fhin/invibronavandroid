package com.fhin.invibronavapp.backendcommunication;

import com.fhin.invibronavapp.models.LocationInformation;
import com.fhin.invibronavapp.models.ShopInformation;
import com.fhin.invibronavapp.models.Waypoint;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.FutureTask;

public interface ICommuncationService extends AutoCloseable {
    public CompletableFuture<Iterable<LocationInformation>> GetAvailableLocations();
    public CompletableFuture<Iterable<ShopInformation>> GetAvailableShopsForLocation(int locationId);
    public CompletableFuture<Iterable<Waypoint>> GetPath(double locationX, double locationY, int locationId, int shopId);
}
