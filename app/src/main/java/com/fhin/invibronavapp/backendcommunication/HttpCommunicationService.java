package com.fhin.invibronavapp.backendcommunication;

import com.fhin.invibronavapp.Constants;
import com.fhin.invibronavapp.models.LocationInformation;
import com.fhin.invibronavapp.models.ShopInformation;
import com.fhin.invibronavapp.models.Waypoint;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.RunnableFuture;
import java.util.logging.Logger;
import java.util.logging.Level;

public class HttpCommunicationService implements ICommuncationService {
    private static volatile HttpCommunicationService instance;
    private static final Object LOCK = new Object();

    private final Map<Integer, LocationInformation> fetchedLocationCache;
    private final Map<Integer, Iterable<ShopInformation>> shopsPerLocationCache;
    private final InVibroConfig config;
    private final Logger logger;

    protected HttpCommunicationService(){
        fetchedLocationCache = new HashMap<>();
        shopsPerLocationCache = new HashMap<>();
        this.config = new InVibroConfig(Constants.DEFAULT_HOSTNAME, new HashMap<>());
        logger = Logger.getLogger(HttpCommunicationService.class.getName(), null);
    }

    public static ICommuncationService getInstance(){
        if (instance == null){
            synchronized (LOCK){
                if (instance == null){
                    instance = new HttpCommunicationService();
                }
            }
        }
        return instance;
    }

    @Override
    public CompletableFuture<Iterable<LocationInformation>> GetAvailableLocations() throws IllegalStateException
    {
        return CompletableFuture.supplyAsync(() -> {
            if (EntityCacheService.getInstance().getCachedLocationCount() == 0){
                Iterable<LocationInformation> fetchedLocationsFromServer;
                try {
                    fetchedLocationsFromServer = FetchLocationsFromServer().get();
                    EntityCacheService.getInstance().UpdateLocationCache(fetchedLocationsFromServer);
                }
                catch (ExecutionException| CancellationException|InterruptedException ex){
                    // TODO: Logging
                    throw new IllegalStateException();
                }
            }
            return EntityCacheService.getInstance().getCachedLocations();
        });
    }

    @Override
    public CompletableFuture<Iterable<ShopInformation>> GetAvailableShopsForLocation(int locationId) throws IllegalArgumentException
    {
        return CompletableFuture.supplyAsync(() -> {
            try {
                Iterable<ShopInformation> fetchedShopsForLocationFromServer = FetchShopsForLocation(locationId).get();
                EntityCacheService.getInstance().UpdateShopsForLocationCache(locationId, fetchedShopsForLocationFromServer);
                return fetchedShopsForLocationFromServer;
            }
            catch (ExecutionException| CancellationException|InterruptedException ex){
                // TODO: Logging
                throw new IllegalArgumentException();
            }
        });
    }

    @Override
    public CompletableFuture<Iterable<Waypoint>> GetPath(double locationX, double locationY, int locationId, int shopId) throws IllegalArgumentException
    {
        return CompletableFuture.supplyAsync(() -> Arrays.asList(new Waypoint[0]));
    }

    @Override
    public void close() {

    }

    private RunnableFuture<Iterable<LocationInformation>> FetchLocationsFromServer(){
        RunnableFuture<Iterable<LocationInformation>> fetchLocationsFutureTask = new FutureTask<>(() -> {
            Iterable<LocationInformation> fetchedLocations = Arrays.asList();

            try {
                String requestUrl = this.config.GetUrlForRequestType(InVibroConfig.REQUEST_TYPE.ALL_LOCATIONS);
                URL url = new URL(requestUrl);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    urlConnection.setDoInput(true);
                    urlConnection.setRequestProperty("Accept", "application/json");
                    urlConnection.connect();

                    try (BufferedReader bufferedResponseReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()))) {
                        Gson gson = new Gson();
                        Type locationInformationCollectionType = new TypeToken<Iterable<LocationInformation>>() {
                        }.getType();
                        fetchedLocations = gson.fromJson(bufferedResponseReader, locationInformationCollectionType);
                    } catch (JsonIOException | JsonSyntaxException ex) {
                        logger.log(Level.SEVERE, Constants.ERR_PARSING_LOCATION_JSON_RESPONSE, ex);
                        throw new IllegalArgumentException(Constants.ERR_PARSING_LOCATION_JSON_RESPONSE);
                    }
                } finally {
                    urlConnection.disconnect();
                }
            } catch (IOException | IllegalArgumentException ex) {
                logger.log(Level.SEVERE, Constants.ERR_FETCHING_LOCATIONS, ex);
                throw new IllegalArgumentException(Constants.ERR_FETCHING_LOCATIONS);
            }
            return fetchedLocations;
        });
        return fetchLocationsFutureTask;
    }

    private RunnableFuture<Iterable<ShopInformation>> FetchShopsForLocation(int locationId){
        RunnableFuture<Iterable<ShopInformation>> fetchShopFutureTask = new FutureTask<>(() -> {
            Iterable<ShopInformation> fetchedShopsForLocation = Arrays.asList();
            try {
                String requestUrl = this.config.GetUrlForRequestType(InVibroConfig.REQUEST_TYPE.SHOPS_FOR_LOCATION);
                // TODO: Correct building of url
                URL url = new URL(requestUrl + "/" + locationId);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    urlConnection.setDoInput(true);
                    urlConnection.setRequestProperty("Accept", "application/json");
                    urlConnection.connect();

                    try (BufferedReader bufferedResponseReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()))){
                        Gson gson = new Gson();
                        Type shopInformationCollectionType = new TypeToken<Iterable<ShopInformation>>(){}.getType();
                        fetchedShopsForLocation = gson.fromJson(bufferedResponseReader, shopInformationCollectionType);
                    }
                    catch (JsonSyntaxException|JsonIOException ex){
                        logger.log(Level.SEVERE, Constants.ERR_PARSING_SHOPS_FOR_LOCATION_JSON_RESPONSE, ex);
                        throw new IllegalArgumentException(Constants.ERR_PARSING_SHOPS_FOR_LOCATION_JSON_RESPONSE);
                    }
                }
                finally{
                    urlConnection.disconnect();
                }
            }
            catch (IOException|IllegalArgumentException ex){
                String errMsg = Constants.BuildMessage(Constants.ERR_FETCHING_SHOPS_FOR_LOCATION, locationId);
                logger.log(Level.SEVERE, errMsg, ex);
                throw new IllegalArgumentException(errMsg);
            }
            return fetchedShopsForLocation;
        });
        return fetchShopFutureTask;
    }
}
