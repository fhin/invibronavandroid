package com.fhin.invibronavapp.backendcommunication;

public class ServiceFactory {
    private static volatile ServiceFactory instance;
    private static final Object LOCK = new Object();

    public static ServiceFactory getInstance(){
        if (instance == null){
            synchronized (LOCK){
                if (instance == null){
                    instance = new ServiceFactory();
                }
            }
        }
        return instance;
    }

    public ICommuncationService getCommunicationService(){
        //return HttpCommunicationService.getInstance();
        return MockupCommunicationService.getInstance();
    }
}
