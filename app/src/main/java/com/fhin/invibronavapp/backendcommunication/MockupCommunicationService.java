package com.fhin.invibronavapp.backendcommunication;

import com.fhin.invibronavapp.models.Direction;
import com.fhin.invibronavapp.models.LocationInformation;
import com.fhin.invibronavapp.models.ShopInformation;
import com.fhin.invibronavapp.models.Waypoint;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class MockupCommunicationService implements ICommuncationService {
    private final int DEFAULT_LOADING_DELAY_IN_MS = 1000;
    private static volatile MockupCommunicationService instance;
    private static final Object LOCK = new Object();

    private Map<Integer, Iterable<ShopInformation>> shopsForLocations;
    private Iterable<LocationInformation> locations;

    protected MockupCommunicationService(){
        locations = new ArrayList<>(
                Arrays.asList(
                        new LocationInformation(0,"Location #1", "Default adress", new byte[0]),
                        new LocationInformation(1,"Location #2", "Default adress that is longer than normal", new byte[0])));

        shopsForLocations = new HashMap<>();
        shopsForLocations.put(
                0, new ArrayList<>(
                        Arrays.asList(
                                new ShopInformation(0, "Test shop 1 for location 1", new byte[0]),
                                new ShopInformation(1, "A very long test shop name to test behaviour for long names for location 1", new byte[0]))));
        shopsForLocations.put(
                1, new ArrayList<>(
                        Arrays.asList(
                                new ShopInformation(0, "Test shop 1 for location 2", new byte[0]),
                                new ShopInformation(1, "Test shop 2 for location 2", new byte[0]),
                                new ShopInformation(2, "Test shop 3 for location 2", new byte[0]),
                                new ShopInformation(3, "Test shop 4 for location 2", new byte[0]),
                                new ShopInformation(4, "Test shop 5 for location 2", new byte[0]),
                                new ShopInformation(5, "Test shop 6 for location 2", new byte[0]),
                                new ShopInformation(6, "A very long test shop name to test behaviour for long names", new byte[0]))));
        EntityCacheService.getInstance().UpdateLocationCache(locations);
        for (LocationInformation location : locations){
            EntityCacheService.getInstance().UpdateShopsForLocationCache(location.getId(), shopsForLocations.get(location.getId()));
        }
    }

    public static ICommuncationService getInstance(){
        if (instance == null){
            synchronized (LOCK){
                if (instance == null){
                    instance = new MockupCommunicationService();
                }
            }
        }
        return instance;
    }

    @Override
    public CompletableFuture<Iterable<LocationInformation>> GetAvailableLocations() {
        return CompletableFuture.supplyAsync(() -> {
                try {
                    Thread.sleep(DEFAULT_LOADING_DELAY_IN_MS);
                }
                catch (InterruptedException ex){ }
                return locations;
        });
    }

    @Override
    public CompletableFuture<Iterable<ShopInformation>> GetAvailableShopsForLocation(int locationId) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(DEFAULT_LOADING_DELAY_IN_MS);
            }
            catch (InterruptedException ex){ }
            if (shopsForLocations.containsKey(locationId)) {
                return shopsForLocations.get(locationId);
            }
            return Arrays.asList();
        });
    }

    @Override
    public CompletableFuture<Iterable<Waypoint>> GetPath(double locationX, double locationY, int locationId, int shopId) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(DEFAULT_LOADING_DELAY_IN_MS);
            }
            catch (InterruptedException ex){ }
            List<Waypoint> waypointList = new ArrayList<>();
            waypointList.add(new Waypoint(Direction.WEST, 7, 5));
            waypointList.add(new Waypoint(Direction.SOUTH, 5, 25));

            //waypointList.add(new Waypoint(Direction.NORTH, 10, 90));
            //waypointList.add(new Waypoint(Direction.SOUTH_EAST, 10, 295));
            //waypointList.add(new Waypoint(Direction.SOUTH_WEST, 10, 225));
            return waypointList;
        });
    }

    @Override
    public void close() throws Exception {

    }
}
