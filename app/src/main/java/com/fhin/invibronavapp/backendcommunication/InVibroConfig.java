package com.fhin.invibronavapp.backendcommunication;

import com.fhin.invibronavapp.Constants;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;

public class InVibroConfig {
    public enum REQUEST_TYPE {
        SHOPS_FOR_LOCATION,
        ALL_LOCATIONS,
        GET_PATH_TO_SHOP
    }

    private String hostName;
    private Map<REQUEST_TYPE, String> requestUrls;

    public InVibroConfig(String hostName, Map<REQUEST_TYPE, String> requestPostfixes){
        SetHost(hostName);
        BuildRequestUrls(requestPostfixes);
    }

    public String GetUrlForRequestType(REQUEST_TYPE requestType){
        if (requestUrls.containsKey(requestType)){
            return this.requestUrls.get(requestType);
        }
        else {
            throw new IllegalArgumentException(Constants.BuildMessage(Constants.ERR_NO_REQUEST_FOR_TYPE, requestType.toString()));
        }
    }

    private void BuildRequestUrls(Map<REQUEST_TYPE, String> requestTypePostfixes){
        for(Map.Entry<REQUEST_TYPE, String> kvPair : requestTypePostfixes.entrySet()){
            if (kvPair.getValue() == null) continue;
            try {
                URL url = new URL(hostName + kvPair.getValue());
                URI uri = url.toURI();
                this.requestUrls.put(kvPair.getKey(), uri.normalize().toString());
            }
            catch (URISyntaxException|MalformedURLException ex) {
                continue;
            }
        }
        this.requestUrls = requestTypePostfixes;
    }

    public void SetHost(String hostName)
    {
        try {
            this.hostName = hostName;
            URL url = new URL(hostName);
            url.toURI();
        }
        catch (URISyntaxException|MalformedURLException ex){
            if (hostName == null){
                hostName = "<null>";
            }
            throw new IllegalArgumentException(Constants.BuildMessage(Constants.ERR_INVALID_URL, hostName));
        }
    }
}
