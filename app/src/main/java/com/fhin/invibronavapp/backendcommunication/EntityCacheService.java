package com.fhin.invibronavapp.backendcommunication;

import com.fhin.invibronavapp.models.LocationInformation;
import com.fhin.invibronavapp.models.ShopInformation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class EntityCacheService {
    private static volatile EntityCacheService instance;
    private static final Object LOCK = new Object();

    // TODO: Use more efficient search structure for iterables values
    private final Map<Integer, LocationInformation> fetchedLocationCache;
    private final Map<Integer, Iterable<ShopInformation>> fetchedShopsPerLocationCache;
    private final Logger logger;

    protected EntityCacheService(){
        fetchedLocationCache = new HashMap<>();
        fetchedShopsPerLocationCache = new HashMap<>();
        logger = Logger.getLogger(EntityCacheService.class.getName(), null);
    }

    public static EntityCacheService getInstance(){
        if (instance == null){
            synchronized (LOCK){
                if (instance == null){
                    instance = new EntityCacheService();
                }
            }
        }
        return instance;
    }

    public Iterable<LocationInformation> getCachedLocations(){
        synchronized (LOCK){
            return fetchedLocationCache.values();
        }
    }

    public int getCachedLocationCount(){
        synchronized (LOCK){
            return fetchedLocationCache.size();
        }
    }

    public LocationInformation getCachedLocation(int locationId){
        synchronized (LOCK){
            if (fetchedLocationCache.containsKey(locationId)){
                return fetchedLocationCache.get(locationId);
            }
        }
        return null;
    }

    public ShopInformation getCachedShopInformation(int locationId, int shopId){
        synchronized (LOCK){
            if (fetchedShopsPerLocationCache.containsKey(locationId)){
                for (ShopInformation cachedShopInfo : fetchedShopsPerLocationCache.get(locationId)){
                    if (cachedShopInfo.getId() == shopId) return cachedShopInfo;
                }
            }
        }
        return null;
    }

    public void UpdateLocationCache(Iterable<LocationInformation> locationsToCache){
        synchronized (LOCK){
            fetchedLocationCache.clear();
            for (LocationInformation locationToCache : locationsToCache){
                if (locationToCache == null) continue;
                fetchedLocationCache.put(locationToCache.getId(), locationToCache);
                fetchedShopsPerLocationCache.put(locationToCache.getId(), new ArrayList<>());
            }
        }
    }

    public void UpdateShopsForLocationCache(int locationId, Iterable<ShopInformation> shopsForLocation){
        List<ShopInformation> filteredShops = new ArrayList<>();
        for (ShopInformation shopToCache : shopsForLocation){
            if (shopToCache == null) continue;
            filteredShops.add(shopToCache);
        }

        synchronized (LOCK) {
            if (!fetchedShopsPerLocationCache.containsKey(locationId)) return;
            fetchedShopsPerLocationCache.remove(locationId);
            fetchedShopsPerLocationCache.put(locationId, filteredShops);
        }
    }
}
