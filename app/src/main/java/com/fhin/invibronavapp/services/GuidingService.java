package com.fhin.invibronavapp.services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import com.fhin.invibronavapp.AxisIndex;
import com.fhin.invibronavapp.Constants;
import com.fhin.invibronavapp.LoggingHelper;
import com.fhin.invibronavapp.R;
import com.fhin.invibronavapp.activities.Notification;
import com.fhin.invibronavapp.activities.guiding.AccelerometerCalibration;
import com.fhin.invibronavapp.activities.guiding.DeviceOrientationHelper;
import com.fhin.invibronavapp.activities.guiding.HeadingChangeListener;
import com.fhin.invibronavapp.activities.guiding.LinearAcceleration;
import com.fhin.invibronavapp.activities.guiding.NavigationHelper;
import com.fhin.invibronavapp.models.Direction;
import com.fhin.invibronavapp.models.DirectionChange;
import com.fhin.invibronavapp.models.Waypoint;
import com.fhin.invibronavapp.other.DirectionChangeWrapper;
import com.google.android.material.transition.MaterialSharedAxis;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class GuidingService extends Service implements SensorEventListener, HeadingChangeListener {
    private String TAG = GuidingService.class.getName();

    private SensorManager sensorManager;
    private Sensor orientationSensor;
    private Sensor accelerationSensor;

    private int currWaypointIdx;
    private ArrayList<Waypoint> pathToDestination;
    private RelativeDistance relativeDistanceToCurrDestination;
    private float requiredAngleToCurrentTarget;
    private boolean headingInRightDirection;

    private NavigationHelper navigationHelper;
    private boolean setInitialWaypoint;
    private LoggingHelper loggingHelper;

    public void pause(){
        Notification.showToast(this, Constants.INFO_SHUTDOWN_SENSOR_SERVICE, true);
        Log.d(TAG, Constants.INFO_PAUSING_SENSOR_LISTENING);
        sensorManager.unregisterListener(this);
    }

    public void resume(){
        Log.d(TAG, Constants.INFO_RESUMING_SENSOR_LISTENING);
        if (orientationSensor != null){
            sensorManager.registerListener(this, orientationSensor, SensorManager.SENSOR_DELAY_NORMAL, SensorManager.SENSOR_DELAY_UI);
        }
        if (accelerationSensor != null){
            sensorManager.registerListener(this, accelerationSensor, SensorManager.SENSOR_DELAY_NORMAL, SensorManager.SENSOR_DELAY_UI);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        // Service provides no binding, so return null
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        currWaypointIdx = 0;
        pathToDestination.clear();
        pathToDestination = intent.getParcelableArrayListExtra(Constants.EXTRA_WAYPOINTS_TO_TARGET);
        headingInRightDirection = false;

        navigationHelper.registerForHeadingChange(this);
        setInitialWaypoint = false;

        String guidingTargetName = intent.getStringExtra(Constants.EXTRA_GUIDING_LOCATION_TARGET_NAME);
        if (guidingTargetName == null){
            guidingTargetName = Constants.INVALID_SHOP_NAME;
        }
        NotificationChannel notificationChannel = new NotificationChannel(
                Constants.GUIDING_SERVICE_NOTIFICATION_CHANNEL_ID,
                Constants.GUIDING_SERVICE_NOTIFICATION_CHANNEL_NAME,
                NotificationManager.IMPORTANCE_LOW);

        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.createNotificationChannel(notificationChannel);

        android.app.Notification notification = new android.app.Notification.Builder(this, Constants.GUIDING_SERVICE_NOTIFICATION_CHANNEL_ID)
                .setContentTitle(Constants.BuildMessage(Constants.GUIDING_SERVICE_NOTIFICATION_TITLE_FORMAT, guidingTargetName))
                //.setContentText(Constants.BuildMessage(Constants.GUIDING_SERVICE_NOTIFICATION_CONTENT_FORMAT, new Object[] {currDirectionAsString, currDirectionDistanceAsString}))
                .setContentText("")
                .setSmallIcon(R.drawable.default_shop_icon)
                .setTicker("test ticker text")
                .build();
        Log.d(TAG, Constants.INFO_STARTING_SENSOR_SERVICE);
        startForeground(Constants.GUIDING_SERVICE_ID, notification);

        // Only start to listen for sensor events after service was started
        resume();
        return START_NOT_STICKY;
    }

    @Override
    public void onCreate(){
        try {
            loggingHelper = new LoggingHelper(this);
        }
        catch (IllegalArgumentException ex){
            Notification.showToast(this, ex.getMessage(), false);
        }

        relativeDistanceToCurrDestination = new RelativeDistance(0,0,0);
        pathToDestination = new ArrayList<>();

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        try {
            orientationSensor = getSensorOfType(Sensor.TYPE_MAGNETIC_FIELD);
            accelerationSensor = getSensorOfType(Sensor.TYPE_ACCELEROMETER);
        }
        catch (IllegalArgumentException ex){
            Log.e(TAG, ex.getMessage());
            throw new IllegalStateException(Constants.ERR_CREATING_SENSOR_SERVICE);
        }

        navigationHelper = new NavigationHelper(loggingHelper.getLogger(),this, Constants.NUM_REQUIRED_ACCELEROMETER_CALIBRATION_MEASUREMENTS);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        pause();
        Notification.showToast(this, Constants.INFO_SHUTDOWN_SENSOR_SERVICE, true);
        Log.i(TAG, Constants.INFO_SHUTDOWN_SENSOR_SERVICE);
        if (loggingHelper != null){
            loggingHelper.close();
        }
        stopForeground(true);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        logMeasurement(event.sensor.getType(), event.values);
        navigationHelper.handleSensorEvent(event);

        switch (event.sensor.getType()){
            case Sensor.TYPE_ACCELEROMETER:
                if (!navigationHelper.isSetupDone()){
                    break;
                }
                if (!setInitialWaypoint) {
                    setNewRelativeTarget(0);
                    setInitialWaypoint = true;
                }
                updateDistanceToCurrentTarget();
                break;
            default:
                break;
        }

        /*



        switch(event.sensor.getType()){
            case Sensor.TYPE_ACCELEROMETER:
                if (!accelerometerCalibration.isDone()){
                    accelerometerCalibration.handleSensorData(event.values);

                    if (accelerometerCalibration.isDone()){
                        float[] calibratedGravity = accelerometerCalibration.getCalibratedValues();
                        currLinearAcceleration .updateCalibratedGravity(calibratedGravity[0], calibratedGravity[1], calibratedGravity[2]);
                        Log.d(TAG, String.format("Calibrated gravity: X: %f, Y: %f, Z: %f", new Object[]{calibratedGravity[0], calibratedGravity[1], calibratedGravity[2]}));
                        deviceOrientationHelper.UpdateAccelerometerReadings(event.values);

                        // TODO:
                        requiredAngleToCurrentTarget = 90;
                        setNewRelativeTarget(currWaypointIdx);
                        triggerDirectionChange(pathToDestination.get(currWaypointIdx).getDirection());
                    }
                }
                else {
                    currLinearAcceleration.transformSensorData(event.values[0], event.values[1], event.values[2]);
                    if (!currLinearAcceleration.wasSignificantMovement()){
                        break;
                    }

                    Log.d(TAG, String.format("Measured: X: %f, Y: %f, Z: %f", new Object[]{currLinearAcceleration.X, currLinearAcceleration.Y, currLinearAcceleration.Z}));
                    deviceOrientationHelper.UpdateAccelerometerReadings(event.values);
                    //deviceOrientationHelper.UpdateAccelerometerReadings(new float[]{ currLinearAcceleration.X, currLinearAcceleration.Y, currLinearAcceleration.Z});
                    if (deviceOrientationHelper.GetAzimuth() <= requiredAngleToCurrentTarget + Constants.ALLOWED_TARGET_ANGLE_DEVIATION
                            && deviceOrientationHelper.GetAzimuth() >= requiredAngleToCurrentTarget - Constants.ALLOWED_TARGET_ANGLE_DEVIATION)
                    {
                        if (!headingInRightDirection){
                            headingInRightDirection = true;
                            triggerDirectionChange(Direction.NORTH);
                        }
                    }
                    else {
                        // TODO: Direct user to turn in the right direction
                        if (headingInRightDirection){
                            headingInRightDirection = false;
                            Log.d(TAG, String.format("Need to be facing %f degrees but is actually %f", requiredAngleToCurrentTarget, deviceOrientationHelper.GetAzimuth()));
                        }
                    }
                    updateDistanceToCurrentTarget(currLinearAcceleration.X, currLinearAcceleration.Y, currLinearAcceleration.Z);
                }
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                deviceOrientationHelper.UpdateMagnetometerReadings(event.values);
                break;
            default:
                break;
        }
         */
    }

    private void logMeasurement(int sensorType, float[] values){
        String[] printableMeasurements = new String[]{
                String.valueOf(values[AxisIndex.X.index]),
                String.valueOf(values[AxisIndex.Y.index]),
                String.valueOf(values[AxisIndex.Z.index])
        };
        String measurementText = String.join(",", printableMeasurements);
        switch (sensorType){
            case Sensor.TYPE_ACCELEROMETER:
                loggingHelper.getLogger().info(String.format(Constants.LOG_ENTRY_DATA_FORMAT, new Object[]{"M", "ACC", "RAW", measurementText}));
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                //loggingHelper.getLogger().info(String.format(Constants.LOG_ENTRY_DATA_FORMAT, new Object[]{"M", "MAG", "RAW", measurementText}));
                break;
            default:
                break;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        switch (sensor.getType()){
            case Sensor.TYPE_ACCELEROMETER:
            case Sensor.TYPE_GYROSCOPE:
                String infoMsg = Constants.BuildMessage(Constants.INFO_SENSOR_ACCURACY_CHANGED, new Object[]{sensor.getName(), sensor.getType(), accuracy});
                Log.i(TAG, infoMsg);
                break;
            default:
                break;
        }
    }

    private Sensor getSensorOfType(int sensorType){
        if (sensorManager.getDefaultSensor(sensorType) != null){
            return sensorManager.getDefaultSensor(sensorType);
        }
        else {
            String errMsg = Constants.BuildMessage(Constants.ERR_NO_SENSOR_FOR_TYPE, sensorType);
            Log.d(TAG, errMsg);
            throw new IllegalArgumentException(errMsg);
        }
    }

    private synchronized void triggerDirectionChange(boolean forDevice, Direction newDirection){
        DirectionChangeWrapper.getInstance().setNewDirection(forDevice, newDirection);
    }

    // TODO:
    private void updateDistanceToCurrentTarget(){
        final BigDecimal[] currentMovement = navigationHelper.getCurrentMovement();

        final BigDecimal[] angleTransformedMovement = new BigDecimal[3];
        angleTransformedMovement[AxisIndex.Y.index] = currentMovement[AxisIndex.Y.index];

        Log.d(TAG, String.format("Curr. velocity: X: %f, Y: %f, Z: %f", new Object[]{currentMovement[0].floatValue(), currentMovement[1].floatValue(), currentMovement[2].floatValue()}));

        if (!relativeDistanceToCurrDestination.update(currentMovement[AxisIndex.X.index], currentMovement[AxisIndex.Y.index], currentMovement[AxisIndex.Z.index])){
            float[] test = relativeDistanceToCurrDestination.getDistanceLeft();
            Log.d(TAG, String.format("Distance left: X: %f, Y: %f, Z: %f", new Object[]{test[0], test[1], test[2]}));
            return;
        }

        currWaypointIdx++;
        if (currWaypointIdx >= pathToDestination.size()){
            return;
        }
        Notification.showToast(this, String.format("Reached goal: New Direction %s", pathToDestination.get(currWaypointIdx).getDirection().toString()), true);
        setNewRelativeTarget(currWaypointIdx);
    }

    private void setNewRelativeTarget(int currWaypointIdx){
        if (currWaypointIdx < 0 || currWaypointIdx >= pathToDestination.size()) return;

        float airDistanceToTarget = (float) pathToDestination.get(currWaypointIdx).getDistance();
        double angleToTarget = pathToDestination.get(currWaypointIdx).getAngle();
        // TODO: Could also store angle in waypoint in radiants
        double angleInRadiants = Math.toRadians(angleToTarget);

        float relativeDistanceY = (float) (Math.cos(angleInRadiants) * airDistanceToTarget);
        float relativeDistanceX = (float ) (Math.sin(angleInRadiants) * airDistanceToTarget);
        float relativeDistanceZ = 0;

        float[] overshootToCurrTarget = relativeDistanceToCurrDestination.getOvershoot();
        relativeDistanceX += overshootToCurrTarget[AxisIndex.X.index];
        relativeDistanceY += overshootToCurrTarget[AxisIndex.Y.index];
        requiredAngleToCurrentTarget += pathToDestination.get(currWaypointIdx).getAngle() % 360;
        relativeDistanceToCurrDestination.setNewRelativeTarget(relativeDistanceX, relativeDistanceY, relativeDistanceZ);

        // Trigger new direction change to all observers
        triggerDirectionChange(false, pathToDestination.get(currWaypointIdx).getDirection());
    }

    @Override
    public void onHeadingChanged(DirectionChange directionChange) {
        triggerDirectionChange(directionChange.forDevice, directionChange.newDirection);
    }
}
