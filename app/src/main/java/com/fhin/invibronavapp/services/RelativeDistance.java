package com.fhin.invibronavapp.services;

import android.util.Log;

import com.fhin.invibronavapp.AxisIndex;
import com.fhin.invibronavapp.Constants;

import java.math.BigDecimal;

public class RelativeDistance {
    private final BigDecimal[] currentTarget;
    private final BigDecimal[] currentRelativePosition;
    private final BigDecimal[] currentMovement;

    public RelativeDistance(float relativeDistanceX, float relativeDistanceY, float relativeDistanceZ){
        currentTarget = new BigDecimal[]{
                BigDecimal.valueOf(relativeDistanceX),
                BigDecimal.valueOf(relativeDistanceY),
                BigDecimal.valueOf(relativeDistanceZ),
        };
        currentRelativePosition = new BigDecimal[] {
                BigDecimal.valueOf(0),
                BigDecimal.valueOf(0),
                BigDecimal.valueOf(0)};

        currentMovement= new BigDecimal[] {
                BigDecimal.valueOf(0),
                BigDecimal.valueOf(0),
                BigDecimal.valueOf(0)};
    }

    // TODO:
    public boolean update(BigDecimal deltaX, BigDecimal deltaY, BigDecimal deltaZ){
        Log.d("DIST", String.format("Moving: X %f, Y %f, Z %f", new Object[]{deltaX.floatValue(), deltaY.floatValue(), deltaZ.floatValue()}));
        boolean wentDistanceX = handleAxisDistanceUpdate(deltaX, AxisIndex.X.index);
        boolean wentDistanceY = handleAxisDistanceUpdate(deltaY, AxisIndex.Y.index);
        boolean wentDistanceZ = true; //handleAxisDistanceUpdate(deltaZ, AxisIndex.Z.index);
        return wentDistanceX & wentDistanceY & wentDistanceZ;
    }

    public float[] getOvershoot(){
        float distanceX = currentTarget[AxisIndex.X.index].subtract(currentRelativePosition[AxisIndex.X.index]).floatValue();
        float distanceY = currentTarget[AxisIndex.Y.index].subtract(currentRelativePosition[AxisIndex.Y.index]).floatValue();
        float distanceZ = currentTarget[AxisIndex.Z.index].subtract(currentRelativePosition[AxisIndex.Z.index]).floatValue();

        if (distanceX > 0){
            distanceX = 0;
        }

        if (distanceY > 0){
            distanceY = 0;
        }

        if (distanceZ > 0){
            distanceZ = 0;
        }
        return new float[]{ distanceX, distanceY, distanceZ };
    }

    public float[] getDistanceLeft(){
        return new float[]{
                currentTarget[AxisIndex.X.index].subtract(currentRelativePosition[AxisIndex.X.index]).floatValue(),
                currentTarget[AxisIndex.Y.index].subtract(currentRelativePosition[AxisIndex.Y.index]).floatValue(),
                currentTarget[AxisIndex.Z.index].subtract(currentRelativePosition[AxisIndex.Z.index]).floatValue()
        };
    }

    public void setNewRelativeTarget(float relativeDistanceX, float relativeDistanceY, float relativeDistanceZ){
        currentTarget[AxisIndex.X.index] = currentRelativePosition[AxisIndex.X.index].add(BigDecimal.valueOf(relativeDistanceX));
        currentTarget[AxisIndex.Y.index] = currentRelativePosition[AxisIndex.Y.index].add(BigDecimal.valueOf(relativeDistanceY));
        currentTarget[AxisIndex.Z.index] = currentRelativePosition[AxisIndex.Z.index].add(BigDecimal.valueOf(relativeDistanceZ));
    }

    // TODO:
    private boolean handleAxisDistanceUpdate(BigDecimal axisMovement, int axisIdx){
        final BigDecimal previousPosition = currentRelativePosition[axisIdx];
        final BigDecimal newPosition = previousPosition.add(axisMovement);
        final BigDecimal movementDelta = newPosition.subtract(previousPosition);
        currentMovement[axisIdx] = movementDelta;
        currentRelativePosition[axisIdx] = newPosition;

        final BigDecimal distanceLeft = currentTarget[axisIdx].subtract(newPosition);
        return distanceLeft.abs().compareTo(BigDecimal.valueOf(Constants.WAYPOINT_REACHED_THRESHOLD)) <= 0;
    }

    public boolean wasSignificantMovement(){
        return isCurrentAxisTranslationSignificant(currentMovement[AxisIndex.X.index]) || isCurrentAxisTranslationSignificant(currentMovement[AxisIndex.Y.index]) || isCurrentAxisTranslationSignificant(currentMovement[AxisIndex.Z.index]);
    }

    private boolean isCurrentAxisTranslationSignificant(BigDecimal axisMovement){
        return axisMovement.abs().compareTo(BigDecimal.valueOf(Constants.SIGNIFICANT_AXIS_MOVEMENT_THRESHOLD)) > 0;
    }
}
