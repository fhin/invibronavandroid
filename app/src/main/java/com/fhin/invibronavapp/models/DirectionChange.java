package com.fhin.invibronavapp.models;

public class DirectionChange {
    public final boolean forDevice;
    public final Direction previousDirection;
    public final Direction newDirection;

    public DirectionChange(boolean forDevice, Direction previousDirection, Direction newDirection){
        this.forDevice = forDevice;
        this.previousDirection = previousDirection;
        this.newDirection = newDirection;
    }
}
