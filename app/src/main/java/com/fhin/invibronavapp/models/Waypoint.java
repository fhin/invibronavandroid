package com.fhin.invibronavapp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Waypoint implements Parcelable {
    private Direction direction;
    private double angle;
    private double distance;

    public Waypoint(Direction direction, double distance, double angle){
        setDirection(direction);
        setDistance(distance);
        setAngle(angle);
    }

    public Direction getDirection(){
        return direction;
    }

    public void setDirection(Direction direction){
        this.direction = direction;
    }

    public double getDistance(){
        return distance;
    }

    public void setDistance(double distance){
        this.distance = distance;
    }

    public void setAngle(double angle) {this.angle = angle; }
    public double getAngle(){return angle;}

    protected Waypoint(Parcel in) {
        angle = in.readDouble();
        distance = in.readDouble();
        direction = Direction.getDirectionForFlag(in.readByte());
    }

    public static final Creator<Waypoint> CREATOR = new Creator<Waypoint>() {
        @Override
        public Waypoint createFromParcel(Parcel in) {
            return new Waypoint(in);
        }

        @Override
        public Waypoint[] newArray(int size) {
            return new Waypoint[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(angle);
        dest.writeDouble(distance);
        dest.writeByte(direction.byteValue);
    }
}
