package com.fhin.invibronavapp.models;

public class ShopInformation {
    private int id;
    private String name;
    private byte[] icon;

    public ShopInformation(int id, String name, byte[] icon){
        setId(id);
        setName(name);
        setIcon(icon);
    }

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public byte[] getIcon(){
        return icon;
    }

    public void setIcon(byte[] icon){
        this.icon = icon;
    }
}
