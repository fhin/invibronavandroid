package com.fhin.invibronavapp.models;

public class LocationInformation {
    private int id;
    private String name;
    private String adress;
    private byte[] icon;

    public LocationInformation(int id, String name, String adress, byte[] icon){
        setId(id);
        setName(name);
        setAdress(adress);
        setIcon(icon);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public byte[] getIcon() {
        return icon;
    }

    public void setIcon(byte[] icon) {
        this.icon = icon;
    }
}
