package com.fhin.invibronavapp.models;

import com.fhin.invibronavapp.Constants;

public enum Direction {
    NORTH(Constants.DIRECTION_NORTH_FLAG),
    NORTH_EAST(Constants.DIRECTION_NORTHEAST_FLAG),
    EAST(Constants.DIRECTION_EAST_FLAG),
    SOUTH_EAST(Constants.DIRECTION_SOUTHEAST_FLAG),
    SOUTH(Constants.DIRECTION_SOUTH_FLAG),
    SOUTH_WEST(Constants.DIRECTION_SOUTHWEST_FLAG),
    WEST(Constants.DIRECTION_WEST_FLAG),
    NORTH_WEST(Constants.DIRECTION_NORTHWEST_FLAG),
    INVALID(Constants.DIRECTION_INVALID_FLAG);

    public final byte byteValue;

    Direction(byte byteValue){
        this.byteValue = byteValue;
    }

    public static Direction getDirectionForFlag(byte directionByteFlag){
        switch (directionByteFlag){
            case Constants.DIRECTION_NORTH_FLAG:
                return Direction.NORTH;
            case Constants.DIRECTION_NORTHEAST_FLAG:
                return Direction.NORTH_EAST;
            case Constants.DIRECTION_EAST_FLAG:
                return Direction.EAST;
            case Constants.DIRECTION_SOUTHEAST_FLAG:
                return Direction.SOUTH_EAST;
            case Constants.DIRECTION_SOUTH_FLAG:
                return Direction.SOUTH;
            case Constants.DIRECTION_SOUTHWEST_FLAG:
                return Direction.SOUTH_WEST;
            case Constants.DIRECTION_WEST_FLAG:
                    return Direction.WEST;
            case Constants.DIRECTION_NORTHWEST_FLAG:
                return Direction.NORTH_WEST;
            default:
                return Direction.INVALID;
        }
    }

    public static Direction getDirectionFromAngle(double angleInDegrees){
        Direction directionForAngle = Direction.INVALID;
        if (angleInDegrees < 0 || angleInDegrees > 360){
            return directionForAngle;
        }

        Direction[] directionArray = new Direction[]{Direction.EAST, Direction.NORTH_EAST, Direction.NORTH, Direction.NORTH_WEST, Direction.WEST, Direction.SOUTH_WEST, Direction.SOUTH, Direction.SOUTH_EAST};
        try {
            int directionIdx = (int) (angleInDegrees / 45);
            if (directionIdx >= 0 && directionIdx < directionArray.length){
                directionForAngle = directionArray[directionIdx];
            }
        }
        catch (Exception ex){
            // TODO: Logging
        }
        finally {
            return directionForAngle;
        }
    }
}
