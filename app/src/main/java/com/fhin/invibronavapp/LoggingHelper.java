package com.fhin.invibronavapp;

import android.content.Context;
import android.renderscript.ScriptGroup;

import com.fhin.invibronavapp.activities.Notification;
import com.fhin.invibronavapp.services.GuidingService;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class LoggingHelper {
    private Logger logger;
    private final FileHandler logFileHandler;

    public LoggingHelper(Context context){
        final SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss");
        final String LOG_PATH = context.getExternalCacheDir().getPath();
        final String LOG_FILENAME = String.format("log-%s", formatter.format(new Date())) + ".log";

        Path logfilePath = Paths.get(LOG_PATH, LOG_FILENAME);

        try {
            /*
            System.clearProperty("java.util.logging.SimpleFormatter.format");
            if (System.getProperty("java.util.logging.SimpleFormatter.format") != null){
                System.clearProperty("java.util.logging.SimpleFormatter.format");
            }

            System.setProperty("java.util.logging.SimpleFormatter.format", Constants.GENERAL_LOG_ENTRY_FORMAT);
             */

            /*
            logFileHandler = new FileHandler(logfilePath.toAbsolutePath().toString());
            logFileHandler.setFormatter(new SimpleFormatter());

            logger = Logger.getLogger(GuidingService.class.getName(), null);

            InputStream logConfig = context.getResources().openRawResource(R.raw.log);
            LogManager logManager = LogManager.getLogManager();
            logManager.readConfiguration(logConfig);
            logManager.addLogger(logger);
            logger = logManager.getLogger(GuidingService.class.getName());
            logger.info(Constants.LOG_HEADER_LINE);
            logger.addHandler(logFileHandler);
             */
            System.setProperty("java.util.logging.SimpleFormatter.format", Constants.GENERAL_LOG_ENTRY_FORMAT);
            LogManager.getLogManager().reset();

            logFileHandler = new FileHandler(logfilePath.toAbsolutePath().toString());
            logFileHandler.setFormatter(new SimpleFormatter());

            logger = Logger.getLogger(GuidingService.class.getName(), null);
            logger.setUseParentHandlers(false);
            logger.addHandler(logFileHandler);

            logger.info(Constants.LOG_HEADER_LINE);
            Notification.showToast(context, logfilePath.toAbsolutePath().toString(), false);
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("ERROR setting up logging @ " + logfilePath.toAbsolutePath().toString());
        }
    }

    public void close(){
        if (logFileHandler != null){
            if (logger != null){
                logger.removeHandler(logFileHandler);
            }
            logFileHandler.close();
        }
    }

    public final Logger getLogger(){
        return logger;
    }
}
